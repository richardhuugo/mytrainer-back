import * as nodemailer from 'nodemailer';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import TpUser from '../models/User/Tipo';
import User from '../models/User/User';
import Empresa from '../models/Empresa/Empresa';
import Funcionario from '../models/Funcionario/Funcionario';
import EmpresaFuncionario from '../models/Empresa/EmpresaFuncionario';
import EmpresaAluno from '../models/Empresa/EmpresaAluno';
import { gerarPassword, transporter, sendErrorsFromDB } from '../config/utils';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import * as bcrypt from 'bcrypt';
import Aluno from '../models/Aluno/Aluno';

const emailRegex = /\S+@\S+\.\S+/
const passwordRegex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})/


export const listarAlunoAcademia = (req, res) => {
    EmpresaAluno.find({ empresa_id: req.decoded._id })
        .populate({ path: 'aluno_id', populate: { path: 'user' } })
        .exec((error, result) => {
            if (error) {
                res.status(500).json({ errors: [error] })
            } else {
                res.json(result)
            }
        });
}

export const registrarAlunoAcademia = (req, res) => {
    const aluno_id = req.body.aluno || ''
    const nome = req.body.nome || ''
    const sobrenome = req.body.sobrenome || ''
    const email = req.body.email || ''

    const empresa_id = req.body.empresa || req.decoded._id

    if (aluno_id != "") {
        User.findOne({ _id: aluno_id }, (error, sucesso) => {
            if (error) {
                return sendErrorsFromDB(res, error);
            } else if (sucesso) {
                const saveAlunoEmpresa = new EmpresaAluno({
                    empresa_id,
                    aluno_id: sucesso._id
                })
                saveAlunoEmpresa.save(error => {
                    if (error) {
                        return sendErrorsFromDB(res, error);
                    }
                    // aluno registrado com sucesso.
                    res.status(200).send({ message: 'Aluno registrado com sucesso! ' })
                })
            } else {
                return res.status(400).send({ message: 'Não encontramos nenhum Aluno registrado! ' })
            }
        })
    } else {
        // cria novo user do tipo aluno registrando-o na academia
        User.findOne({ email }, (error, sucesso) => {
            if (error) {
                return sendErrorsFromDB(res, error);
            } else if (sucesso) {
                return res.status(400).send({ message: 'O email já está sendo utilizado! ' })
            } else {
                TpUser.findOne({ tipo: ALUNO }, (error, sucesso) => {
                    if (error) {
                        return sendErrorsFromDB(res, error);
                    } else {

                        const salt = bcrypt.genSaltSync()
                        const passwordHash = bcrypt.hashSync('123456', salt)

                        const saveUser = new User({
                            nome,
                            sobrenome,
                            email,
                            senha: passwordHash,
                            tp_user: sucesso._id
                        })
                        saveUser.save(error => {
                            if (error) {
                                return sendErrorsFromDB(res, error);
                            }
                            const saveAluno = new Aluno({
                                user: saveUser._id,
                                descricao_pessoal: ''
                            })

                            saveAluno.save(erroAluno => {
                                if (erroAluno) {
                                    return sendErrorsFromDB(res, error);
                                }

                                new EmpresaAluno({
                                    empresa_id,
                                    aluno_id: saveAluno._id
                                }).save(empresaError => {
                                    if (empresaError) {
                                        return sendErrorsFromDB(res, error);
                                    }
                                    res.status(200).send({ message: 'Aluno registrado com sucesso ! Um email foi enviado o aluno contendo a senha de acesso.' })
                                })
                            })


                        })
                    }
                })
            }
        })
    }
}


// ==========================  FUNCIONARIO EMPRESA ========================================================

export const registrarFuncionarioEmpresa = (req, res) => {
    const id_user = req.body.user || ''
    var empresa_id = req.body.empresa || ''
    const nome = req.body.nome || ''
    const sobrenome = req.body.sobrenome || ''
    const email = req.body.email || ''
    empresa_id == "" ? empresa_id = req.decoded._id : empresa_id = empresa_id
    const salt = bcrypt.genSaltSync()
    const passwordHash = bcrypt.hashSync(gerarPassword(), salt)

    const verificar = req.decoded
    if (verificar.tp_user.tp_user == "Empresa" || verificar.tp_user.tp_user == "Admin") {
        if (verificar.tp_user.tp_user == "Empresa") {
            if (empresa_id == "") {
                return res.status(400).json({ error: 'E necessario enviar os dados da empresa.' })
            }
            if (id_user != "") {
                Funcionario.findOne({ _id: id_user }, (error, funcio) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (funcio) {// registrar funcionario na empresa   
                        Empresa.findOne({ user: verificar._id }, (error, empresa) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            }
                            EmpresaFuncionario.findOne({
                                empresa_id: empresa._id,
                                funcionario_id: funcio._id
                            }, (error, sucesso) => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                } else if (sucesso) {

                                    return res.status(400).send({ message: 'O funcionario já se encontra registrado nessa empresaa. ' + sucesso })
                                } else {
                                    const regEmpresaFuncionario = new EmpresaFuncionario({
                                        empresa_id: empresa._id,
                                        funcionario_id: funcio._id,
                                        status: 'A'
                                    })

                                    regEmpresaFuncionario.save((error) => {
                                        if (error) {
                                            return sendErrorsFromDB(res, error)
                                        }
                                        return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                                    })
                                }
                            })

                        })


                    } else {// registrar funcionario e registrar na empresa

                        const registrarFuncionario = new Funcionario({
                            user_id: id_user,
                            descricao_pessoal: '',
                            status: 'A'
                        })

                        registrarFuncionario.save((error) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            }

                            const regEmpresaFuncionario = new EmpresaFuncionario({
                                empresa_id: verificar._id,
                                funcionario_id: registrarFuncionario._id,
                                status: 'A'
                            })

                            regEmpresaFuncionario.save((error) => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                }
                                return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                            })
                        })
                    }
                })
            } else {

                User.findOne({ email }, (error, sucesso) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (sucesso) {
                        return res.status(400).send({ error: 'O usuário informado se encontra registrado em nosso sistema, tente a opção de usar usuário registado.' })
                    } else {
                        TpUser.findOne({ tp_user: 'Funcionario' }, (error, tp) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            } else {
                                const pass = gerarPassword();

                                const salt = bcrypt.genSaltSync()
                                const passwordHash = bcrypt.hashSync(pass, salt)
                                const registerUser = new User({
                                    nome,
                                    sobrenome,
                                    email,
                                    senha: passwordHash,
                                    tp_user: tp._id
                                })
                                // registrar o usuario
                                registerUser.save((error) => {
                                    if (error) {
                                        return sendErrorsFromDB(res, error)
                                    } else {
                                        //registrar funcionario
                                        const registrarFuncionario = new Funcionario({
                                            user_id: registerUser._id,
                                            descricao_pessoal: '',
                                            status: 'A'
                                        })

                                        registrarFuncionario.save((error) => {
                                            if (error) {
                                                return sendErrorsFromDB(res, error)
                                            }
                                            Empresa.findOne({ user: verificar._id }, (error, empresa) => {

                                                if (error) {
                                                    return sendErrorsFromDB(res, error)
                                                }

                                                const regEmpresaFuncionario = new EmpresaFuncionario({
                                                    empresa_id: empresa._id,
                                                    funcionario_id: registrarFuncionario._id,
                                                    status: 'A'
                                                })

                                                regEmpresaFuncionario.save((error) => {
                                                    if (error) {
                                                        return sendErrorsFromDB(res, error)
                                                    }
                                                    return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                                                })
                                            })

                                        })
                                    }
                                })
                            }
                        })

                    }
                })
            }
        } else {
            if (empresa_id == "") {
                return res.status(400).json({ message: 'E necessario enviar os dados da empresa.' })
            }
            if (id_user != "") {
                Funcionario.findOne({ user_id: id_user }, (error, funcio) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (funcio) {// registrar funcionario na empresa   
                        EmpresaFuncionario.findOne({
                            empresa_id: empresa_id,
                            funcionario_id: funcio._id
                        }, (error, sucesso) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            } else if (sucesso) {
                                return res.status(400).send({ message: 'O funcionario já se encontra registrado nessa empresa.' })
                            } else {
                                const regEmpresaFuncionario = new EmpresaFuncionario({
                                    empresa_id: empresa_id,
                                    funcionario_id: funcio._id,
                                    status: 'A'
                                })

                                regEmpresaFuncionario.save((error) => {
                                    if (error) {
                                        return sendErrorsFromDB(res, error)
                                    }
                                    return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                                })
                            }
                        })

                    } else {// registrar funcionario e registrar na empresa

                        const registrarFuncionario = new Funcionario({
                            user_id: id_user,
                            descricao_pessoal: '',
                            status: 'A'
                        })

                        registrarFuncionario.save((error) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            }

                            const regEmpresaFuncionario = new EmpresaFuncionario({
                                empresa_id: empresa_id,
                                funcionario_id: registrarFuncionario._id,
                                status: 'A'
                            })

                            regEmpresaFuncionario.save((error) => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                }
                                return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                            })
                        })
                    }
                })
            } else {

                User.findOne({ email }, (error, sucesso) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (sucesso) {
                        return res.status(400).send({ message: 'O usuário informado se encontra registrado em nosso sistema, tente a opção de usar usuário registado.' })
                    } else {
                        TpUser.find({ tp_user: 'Funcionario' }, (error, tp) => {
                            if (error) {
                                return sendErrorsFromDB(res, error)
                            } else {
                                const registerUser = new User({
                                    nome,
                                    sobrenome,
                                    email,
                                    senha: passwordHash,
                                    tp_user: tp._id
                                })


                                // registrar o usuario
                                registerUser.save((error) => {
                                    if (error) {
                                        return sendErrorsFromDB(res, error)
                                    } else {
                                        //registrar funcionario
                                        const registrarFuncionario = new Funcionario({
                                            user_id: registerUser._id,
                                            descricao_pessoal: ''

                                        })

                                        registrarFuncionario.save((error) => {
                                            if (error) {
                                                return sendErrorsFromDB(res, error)
                                            }

                                            const id = mongoose.Types.ObjectId(empresa_id);

                                            const regEmpresaFuncionario = new EmpresaFuncionario({
                                                empresa_id: id,
                                                funcionario_id: registrarFuncionario._id,
                                                status: 'A'
                                            })

                                            regEmpresaFuncionario.save((error) => {
                                                if (error) {
                                                    return sendErrorsFromDB(res, error)
                                                }
                                                return res.status(200).send({ message: 'Funcionario registrado com sucesso !', email: 'Os dados de acesso foram enviado para o email cadastrado !' })
                                            })
                                        })
                                    }
                                })
                            }
                        })

                    }
                })
            }
        }


    } else {
        return res.status(400).send({ error: ['unauthorized'] })
    }

}



//===========================================  EMPRESAS ===================================//



export const registrarEmpresa = (req, res, next) => {
    const email = req.body.email || ''
    const nm_fantasia = req.body.nm_fantasia || ''
    const razao_social = req.body.razao_social || ''
    const cnpj = req.body.cnpj || ''
    const cep = req.body.cep || ''
    const cidade = req.body.cidade || ''
    const bairro = req.body.bairro || ''
    const endereco = req.body.endereco || ''

    User.findOne({ email }, (error, usr) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        } else if (usr) {
            return res.status(400).send([{ errors: 'Já existe um usuario registrado com esse endereço de email.' }])
        } else {
            TpUser.findOne({ tipo: 'Empresa' }, (error, tp_user) => {
                if (error) {
                    return sendErrorsFromDB(res, error)
                } else if (tp_user) {
                    const pass = gerarPassword();

                    const salt = bcrypt.genSaltSync()
                    const passwordHash = bcrypt.hashSync(pass, salt)

                    const usersave = new User(
                        {
                            nome: "Empresa ",
                            sobrenome: nm_fantasia,
                            email,
                            senha: passwordHash,
                            tp_user: tp_user._id

                        }
                    )

                    const newEmpresa = new Empresa({
                        nm_fantasia,
                        razao_social,
                        cnpj,
                        cep,
                        cidade,
                        bairro,
                        endereco,
                        user: usersave._id
                    })

                    Empresa.findOne({ cnpj }, (error, empre) => {
                        if (error) {
                            return sendErrorsFromDB(res, error)
                        } else if (empre) {
                            return res.status(400).send([{ errors: 'Existe uma empresa registrada com esse cnpj entre em contato conosco.' }])
                        } else {
                            newEmpresa.save(error => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                } else {

                                    usersave.save(error => {
                                        if (error) {

                                        } else {
                                            const mailOptions = {
                                                from: 'hugorichard2010@gmail.com',
                                                to: usersave.email,
                                                subject: 'myTrainner ',
                                                text: 'Empresa registrada com sucesso! email: ' + email + ' senha: ' + pass
                                            };


                                            transporter.sendMail(mailOptions, function (error, info) {
                                                if (error) {
                                                    return res.status(400).send([{ email: 'Falha ao enviar email, porem seus dados foram salvos com sucesso. entre em contato conosco.' }])
                                                } else {
                                                    console.log('enviado com sucesso')
                                                    return res.status(200).send({ message: 'Empresa registrada com sucesso', email: 'Email enviado com sucesso' })
                                                }
                                            });


                                        }
                                    })



                                }
                            })
                        }
                    })

                } else {
                    return res.status(400).send([{ errors: 'Tipo de usuario nao encontrado.' }])
                }


            })
        }
    })


}
