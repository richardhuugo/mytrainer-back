import * as  _ from 'lodash';
import Pacote from '../models/plano/pacote';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import { strToBool, validar } from '../config/validateAcess';
import {sendErrorsFromDB} from '../config/utils'

/**
 * Registrar pacote de adesão
 *  @param { plano, descricao} req 
 * @param {*} res 
 */

export const registrarPacote = (req, res, next) => {
    const plano = req.body.plano || ''
    const descricao = req.body.descricao || ''
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        const savePacote = new Pacote({
            descricao,
            plano
        })
        savePacote.save(error => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else {
                return res.status(200).send({
                    message: 'Pacote registrado com sucesso !'
                })
            }
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}

export const listarPacote = (req, res, next) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        Pacote.find({})
            .populate('plano')
            .exec((error, result) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                } else {
                    res.json(result)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }


}
