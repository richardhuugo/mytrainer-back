import * as  _ from 'lodash';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import { strToBool, validar } from '../config/validateAcess';
import {sendErrorsFromDB} from '../config/utils';
import Tipo from '../models/User/Tipo';
import User from '../models/User/User';
import Aluno from '../models/Aluno/Aluno';
import Pagamento from '../models/Pagamento/Pagamento';
import EmpresaAluno from '../models/Empresa/EmpresaAluno';
import Funcionario from '../models/Funcionario/Funcionario';
import AlunoFuncionario from '../models/Funcionario/AlunoFuncionario';
import EmpresaFuncionario from '../models/Empresa/EmpresaFuncionario'
import Empresa from '../models/Empresa/Empresa';




const emailRegex= /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const registrarUser = (req, res, next) => {
    const nome = req.body.nome || ''
    const sobrenome = req.body.sobrenome || ''
    const email = req.body.email || ''
    const senha = req.body.senha || ''
    const tipo = req.body.tipo || ''

    if (!email.match(emailRegex)) {
        return res.status(400).send({ errors: ['O e-mail informado está inválido ' + email] })
    }

    const salt = bcrypt.genSaltSync()
    const passwordHash = bcrypt.hashSync(senha, salt)


    User.findOne({ email }, (err, user) => {
        if (err) {
            return sendErrorsFromDB(res, err)
        } else if (user) {
            return res.status(400).send({ error: ['Usuário já cadastrado no sistema'] })
        } else {

            Tipo.findOne({ _id: tipo }, (error, tp) => {

                if (error) {
                    return sendErrorsFromDB(res, error)                     
                }

                if (!tp) {
                    return res.status(400).send({ message: ['Informe um tipo de usuario a ser registrad0o '] });
                }

                const saveUser = new User({
                    nome,
                    sobrenome,
                    email,
                    senha: passwordHash,
                    tipo: tp._id
                });

                if (tp.tipo === ALUNO) {
                    new Aluno({
                        user: saveUser._id
                    }).save(errorALun => {
                        if (error) {
                            return sendErrorsFromDB(res, error)
                        } else {
                            saveUser.save(error => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                } else {
                                    return res.status(200).send({ message: ['Usuario registrado com sucesso'] })
                                }
                            })
                        }
                    })

                } else if (tp.tipo === FUNCIONARIO) {
                    new Funcionario({
                        user_id: saveUser._id
                    }).save(errorFunc => {
                        if (errorFunc) {
                            return sendErrorsFromDB(res, errorFunc)
                        } else {
                            saveUser.save(error => {
                                if (error) {
                                    return sendErrorsFromDB(res, error)
                                } else {
                                    return res.status(200).send({ message: ['Usuario registrado com sucesso'] })
                                }
                            })
                        }
                    });
                } else  if (tp.tipo === ADMIN){
                    saveUser.save(error => {
                        if(error){
                            return sendErrorsFromDB(res, error)
                        }
                        return res.status(200).send({ message: ['Usuario registrado com sucesso'] })
                    })                   
                }else{
                    return res.status(400).send({ message: ['Informe um tipo de usuario a ser registrado ' ] })
                }


            })

        }
    })
}
export const loginUser = (req, res, next) => {
    const email = req.body.email || ''
    const senha = req.body.senha || ''
    User.findOne({ email })
        .populate({ path: 'tipo', model: Tipo })
        .exec((error, user) => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else if (user && bcrypt.compareSync(senha, user.senha)) {
                const dadosEmpresa = user.toObject();
                if (ADMIN == user.tipo.tipo) {
                    administrador(res, user,dadosEmpresa);
                }
                if (ALUNO == user.tipo.tipo) {
                    aluno(res, user,dadosEmpresa);
                }
                if (EMPRESA == user.tipo.tipo) {
                    empresa(res, user,dadosEmpresa);                 
                }
                if (FUNCIONARIO == user.tipo.tipo) {
                    funcionario(res, user,dadosEmpresa);                                        
                }

            } else {
                return res.status(400).send({ errors: ['Usuário/Senha inválidos'] })
            }

        });

}
export const validateToken = (req, res, next) => {
    const token = req.body.token || ''
    jwt.verify(token, process.env.KEY_SERVER.trim(), function (err, decoded) {
        return res.status(200).send({ valid: !err })

    })

}
export const listarUsers = (req, res) => {
    User.find({})
        .populate(['tipo'])

        .exec((error, result) => {
            if (error) {
                res.status(500).json({ errors: [error] })
            } else {
                res.json(result)

            }
        });
}
export const listarFuncionariosEmpresa = (req, res) => {

    var validate = validar(req, [ADMIN, EMPRESA]);
    if (validate.status) {
        if (validate.perfil == ADMIN) {
            EmpresaFuncionario.find({
                status: 'A'
            })
                .populate('funcionario_id')
                .populate('empresa_id')
                .exec((error, result) => {
                    if (error) {
                        res.status(500).json({
                            errors: [error]
                        })
                    } else {

                        User.populate(result, {
                            path: 'funcionario_id.user_id',
                            select: ['nome', 'sobrenome', 'email']
                        }, function (error, updatedUser) {
                            //assuming no error,
                            res.json(updatedUser)
                        });

                    }
                });
        } else if (validate.perfil == EMPRESA) {
            Empresa.findOne({
                user: validate.id
            }, (error, empresa) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                }

                EmpresaFuncionario.find({
                    empresa_id: empresa._id
                })
                    .populate('funcionario_id')
                    .populate('empresa_id')
                    .exec((error, result) => {
                        if (error) {
                            res.status(500).json({
                                errors: [error]
                            })
                        } else {
                            User.populate(result, {
                                path: 'funcionario_id.user_id',
                                select: ['nome', 'sobrenome', 'email']
                            }, function (error, updatedUser) {
                                //assuming no error,
                                res.json(updatedUser)
                            });

                        }
                    });
            })

        }
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }



}


const aluno = (res, user,dadosEmpresa) => {
    Aluno.findOne({ user: user._id }, (error, aluno) => {

        if (error) {
            return sendErrorsFromDB(res, error)
        }
        dadosEmpresa.aluno = aluno
        EmpresaAluno.find({ aluno_id: aluno._id, status: 'A' }, (error, empresaAluno) => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else if (empresaAluno) {
                dadosEmpresa.empresa = empresaAluno

                AlunoFuncionario.find({ aluno_id: aluno._id, status: 'A' }, (error, alunoFuncionario) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (alunoFuncionario) {
                        dadosEmpresa.funcionario = alunoFuncionario

                        const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                            expiresIn: "1h"
                        })

                        return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token })
                    } else {
                        const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                            expiresIn: "1h"
                        })

                        return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token })
                    }
                })
            }
        })
    })
}
const administrador = (res, user,dadosEmpresa) => {
    const token = jwt.sign(user.toJSON(), process.env.KEY_SERVER.trim(), { expiresIn: "1h"})
    return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token })
}
const empresa = (res, user,dadosEmpresa) => {
    Empresa.findOne({ user: user._id }, (error, empresa) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        }

        dadosEmpresa.empresa = empresa;
        const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
            expiresIn: "1h"
        })

        return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token, router: Buffer.from(empresa.nm_fantasia).toString('base64') })

    })
}
const funcionario = (res, user,dadosEmpresa) => {
    Funcionario.findOne({ user_id: user._id }, (error, funcionario) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        } else if (funcionario) {
            dadosEmpresa.funcionario = funcionario;
            EmpresaFuncionario.find({ status: 'A', funcionario_id: funcionario._id })
                .populate('empresa_id')
                .populate('empresa_id')
                .exec((error, empresa) => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else if (empresa) {
                        dadosEmpresa.empresa = empresa;
                        const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                            expiresIn: "1h"
                        })

                        return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token })
                    } else {
                        const token = jwt.sign(dadosEmpresa, process.env.KEY_SERVER.trim(), {
                            expiresIn: "1h"
                        })
                        return res.status(200).send({ nome: user.nome, tp: user.tipo.tipo, token })
                    }
                })
        } else {
            return res.status(400).send({ errors: ['Usuario nao encontrado'] })
        }
    })
}