
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import { strToBool, validar } from '../config/validateAcess';
import {sendErrorsFromDB} from '../config/utils';
import Membro from '../models/Treino/membro';
import Exercicio from '../models/Treino/exercicio';

/**
 * 
 * @param {membro} req 
 * @param {message, error} res 
 * @param {*} next 
 */
export const registrarMembro = (req, res, next) => {
    const membro = req.body.membro || ''

    var validate = validar(req, [ADMIN, FUNCIONARIO, EMPRESA]);

    if (validate.status) {
        Membro.findOne({
            membro
        }, (error, sucesso) => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else if (sucesso) {
                return res.status(400).send({
                    error: ['Membro já cadastrado']
                })
            } else {
                new Membro({
                    membro
                })
                    .save(error => {
                        if (error) {
                            return sendErrorsFromDB(res, error)
                        } else {
                            return res.status(200).send({
                                message: ['Membro registrado com sucesso']
                            })
                        }
                    })
            }
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}
/**
 * 
 * @param {membro} req 
 * @param {*} res 
 */
export const listarMembros = (req, res) => {

    var validate = validar(req, [ADMIN, FUNCIONARIO, EMPRESA]);

    if (validate.status) {
        Membro.find({}, function (err, result) {
            if (err) {
                return sendErrorsFromDB(res, err)
            } else {
                return res.status(200).send(result)
            }
        });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}

/**
  *  FUnção para registrar os exercicios no sistema
  * @param {exercicio, membro} req 
  * @param {*} res 
  * @param {*} next 
  */
 export const registrarExercicio = (req, res, next) => {
    const exercicio = req.body.exercicio || '';
    const membro = req.body.membro || '';

    var update = {
        exercicio,
        membro
    }

    var validate = validar(req, [ADMIN]);

    if (validate.status) {
        Exercicio.findOneAndUpdate(update, update, {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true
        }, function (error, result) {
            if (!error) {
                return res.status(200).send({
                    message: 'Exercicio registrado com sucesso ! '
                })
            } else {
                return res.status(400).send({
                    error
                })
            }

        });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}
/**
 * função para listar os exercicios registrado no sistema
 * @param {*} req 
 * @param {*} res 
 */
export const listarExercicios = (req, res) => {
    var validate = validar(req, [ADMIN, FUNCIONARIO, EMPRESA]);

    if (validate.status) {
        Exercicio.find({})
            .populate('membro')
            .exec((error, result) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                } else {
                    res.json(result)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}