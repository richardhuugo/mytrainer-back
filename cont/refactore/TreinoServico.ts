
import * as  _ from 'lodash';
import { strToBool, validar } from '../config/validateAcess';
import ObjetivoTreino from '../models/Funcionario/ObjetivoTreino';
import { ADMIN, ALUNO, EMPRESA, FUNCIONARIO } from '../config/constants';
import {sendErrorsFromDB} from '../config/utils'

export const registrarObjetivoTreino = (req, res) => {
    const objetivoTreino = req.body.objetivotreino || ''
    var validate = validar(req, [ADMIN]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    var update = { objetivos: objetivoTreino }
    ObjetivoTreino.findOneAndUpdate({ objetivos: objetivoTreino }, update, { upsert: true, new: true, setDefaultsOnInsert: true }, function (error, result) {
        if (!error) {
            // If the document doesn't exist
            if (!result) {
                // Create it
                result = new ObjetivoTreino({ objetivos: objetivoTreino })
            }
            // Save the document
            result.save(function (error) {
                if (!error) {
                    return res.status(200).send({ message: 'Objetivo registrado com sucesso ! ' })
                } else {
                    return sendErrorsFromDB(res, error);
                }
            });
        } else {
            return sendErrorsFromDB(res, error)
        }
    });
}
export const listarObjetivoTreino = (req, res) => {

    var validate = validar(req, [ADMIN, FUNCIONARIO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada'],

        })
    }
    ObjetivoTreino.find()
        .exec((error, result) => {
            if (error) {
                res.status(500).json({
                    errors: [error]
                })
            }
            res.json(result)
        });
}

