import * as _ from 'lodash';
import { FUNCIONARIO, ALUNO, ADMIN, EMPRESA } from '../config/constants'
import * as mongoose from 'mongoose';
import {
    validar,
    strToBool
} from '../config/validateAcess';
import TpUser from '../models/User/Tipo';
import User from '../models/User/User';
import Funcionario from '../models/Funcionario/Funcionario';
import EmpresaFuncionario from '../models/Empresa/EmpresaFuncionario';
import GrupoFuncionario from '../models/Funcionario/GrupoFuncionario';
import GrupoFuncionarioAtividades from '../models/Funcionario/servicos/GrupoFuncionarioAtividades';
import FuncionarioAtividades from '../models/Funcionario/servicos/FuncionarioAtividades';
import Aluno from '../models/Aluno/Aluno';
import AlunoFuncionario from '../models/Funcionario/AlunoFuncionario';
import Pagamento from '../models/Pagamento/Pagamento';

import * as  bcrypt from 'bcrypt'

const sendErrorsFromDB = (res, dbErrors) => {
    const errors = []
    _.forIn(dbErrors.errors, error => errors.push(error.message))
    return res.status(400).json({
        errors
    })
}

export const registrarAlunoFuncionario = (req, res) => {
    const aluno_id = req.body.aluno || ''
    if (aluno_id == "") {
        return res.status(400).send({
            message: 'É necessário informar o Aluno! '
        })
    }
    var validate = validar(req, [FUNCIONARIO]);

    if (strToBool(validate.status)) {
        Aluno.findOne({
            user: aluno_id
        }, (error, alunoFind) => {
            if (error) {
                return sendErrorsFromDB(res, error);
            }

            AlunoFuncionario.findOne({
                aluno_id: alunoFind._id,
                funcionario_id: validate.id
            }, (error, AlunoFun) => {
                if (error) {
                    return sendErrorsFromDB(res, error);
                } else if (AlunoFun) {
                    return res.status(400).send({
                        message: 'Aluno já se encontra registrado com você! '
                    })
                } else {
                    const saveAlunoFuncionario = new AlunoFuncionario({
                        funcionario_id: validate.id,
                        aluno_id: alunoFind._id
                    })
                    saveAlunoFuncionario.save((error) => {
                        if (error) {
                            return sendErrorsFromDB(res, error);
                        }
                        return res.status(200).send({
                            message: 'Aluno registrado com sucesso ! '
                        })
                    })

                }
            })
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}

export const listarAlunoFuncionario = (req, res) => {
    var validate = (validar(req, [FUNCIONARIO]));

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

    AlunoFuncionario.find({ funcionario_id: validate.id }, (error, aluno) => {
        if (error) {
            return sendErrorsFromDB(res, error);
        } else if (aluno) {
            return res.status(200).send({
                aluno
            })
        } else {
            return res.status(200).send({
                message: 'Você não tem nenhum aluno registrado!'
            })
        }
    })

}

export const registrarGrupoFuncionarioAtv = (req, res) => {


    const grupofun_id = req.body.grupofun_id || ''
    const valor = req.body.valor || ''
    const descricao = req.body.descricao || ''
    const obje_treino = req.body.obj_treino || ''

    const objetivoArray = [];

    var validate = (validar(req, [FUNCIONARIO]));

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

    obje_treino.map((item, index) => {
        var find = objetivoArray.indexOf(item._id)

        if (find == -1) {
            objetivoArray.push(item._id)
        }
    })

    const saveGrupoFuncionarioAtividades = new GrupoFuncionarioAtividades({
        grupofun_id,
        obj_treino: objetivoArray,
        valor,
        descricao
    })
    saveGrupoFuncionarioAtividades.save(function (error) {
        if (!error) {
            return res.status(200).send({
                message: 'Atividade registrada com sucesso ! '
            })
        }

        return sendErrorsFromDB(res, error);
    })
}

export const listarGrupoFuncionarioAtividade = (req, res) => {
    var validate = (validar(req, [FUNCIONARIO]));

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    GrupoFuncionarioAtividades.find({})
        .exec((error, result) => {
            if (error) {
                res.status(500).json({
                    errors: [error]
                })
            } else {
                res.json(result)
            }
        });
}

export const registrarGrupoFuncionario = (req, res) => {

    const funcionario = req.body.funcionarios || ''
    const descricao = req.body.descricao || ''

    var validate = validar(req, [FUNCIONARIO]);

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

    const funcionarioArray = [];
    funcionario.map((item, index) => {
        var find = funcionarioArray.indexOf(item._id)

        if (find == -1) {

            funcionarioArray.push(item._id)

        }

    })

    const saveGrupoFuncionario = new GrupoFuncionario({
        funcionario_id: funcionarioArray,
        create_id: validate.id,
        descricao_grupo: descricao
    })

    saveGrupoFuncionario.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error);
        } else {
            return res.status(200).send({
                message: 'Grupo registrado com sucesso ! '
            })
        }
    })

}

export const registrarFuncionarioAtv = (req, res) => {

    var treino_obj = req.body.treino_obj || ""
    const descricao = req.body.descricao || ""
    const valor = req.body.valor || ""

    var validate = validar(req, [FUNCIONARIO]);


    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }


    const saveFuncionarioAtividades = new FuncionarioAtividades({
        funcionario_id: validate.detalhe.funcionario._id,
        valor,
        treino_obj,
        descricao
    })
    saveFuncionarioAtividades.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error)
        }
        return res.status(200).send({
            message: 'Atividades funcionario registrado com sucesso !'
        })
    })
}

export const listarFuncionrioAtividade = (req, res) => {

    FuncionarioAtividades.find({ status: 'A', funcionario_id: req.decoded.funcionario._id })
        .populate({
            path: 'funcionario_id',
            populate: { path: 'user_id', select: ['nome', 'sobrenome', 'email', 'tp_user', 'status'] },

        })
        .exec((error, result) => {
            if (error) {
                return sendErrorsFromDB(res, error)
            }
            res.json(result)
        });
}

export const listarFuncionarios = (req, res) => {

    var validate = (validar(req, [ADMIN, EMPRESA]));

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }


    Funcionario.find({})
        .populate({
            path: 'user_id',
            select: ['sobrenome', 'nome', 'email'],
            model: User

        })
        .exec((error, result) => {
            if (error) {
                res.status(500).json({
                    errors: [error]
                })
            } else {

                EmpresaFuncionario.populate(result, {
                    path: 'funcionario_id._id',
                    select: ['empresa_id']
                }, function (error, updatedUser) {
                    //assuming no error,
                    res.json(updatedUser)
                });
            }
        });
}

export const listarVendas = (req, res) => {

    var validate = (validar(req, [FUNCIONARIO]));

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    Pagamento.find({
        'funcionarioAtv.funcionario_id': {
            $in: [
                mongoose.Types.ObjectId(validate.detalhe.funcionario._id)
            ]
        }
    }, function (error, docs) {
        if (error) {
            return sendErrorsFromDB(res, error)
        }
        res.json(docs)
    });


}

 