/**
export const registrarArea = (req, res) => {

    const funcionario = req.body.funcionario || ''
    const categoria = req.body.categoria || ''
    const numero = req.body.numero || ''

    const token = req.decoded
    const verificar = token._doc;

    if (verificar.tipo.tipo == "Admin" || verificar.tipo.tipo == "Empresa" && funcionario != "") {
        var update = { num_categoria: numero, categoria_id: categoria, funcionario_id: funcionario }
        Area.findOneAndUpdate({ num_categoria: numero, categoria_id: categoria, funcionario_id: funcionario }, update, { upsert: true, new: true, setDefaultsOnInsert: true }, function (error, result) {
            if (!error) {
                // If the document doesn't exist
                if (!result) {
                    // Create it
                    result = new Area({ funcionario_id: funcionario, categoria_id: categoria, num_categoria: numero, status: 'A' })
                }
                // Save the document
                result.save(function (error) {
                    if (!error) {
                        return res.status(200).send({ message: 'Area registrada com sucesso ! ' })
                    } else {
                        return sendErrorsFromDB(res, error);
                    }
                });
            } else {
                return sendErrorsFromDB(res, error)
            }
        });

    } else if (verificar.tipo.tipo == "Funcionario") {
        // return res.status(200).send(token)
        var update = { num_categoria: numero, categoria_id: categoria, funcionario_id: verificar._id }
        Area.findOneAndUpdate({ num_categoria: numero, categoria_id: categoria, funcionario_id: verificar._id }, update, { upsert: true, new: true, setDefaultsOnInsert: true }, function (error, result) {
            if (!error) {
                // If the document doesn't exist
                if (!result) {
                    // Create it
                    result = new Area({ funcionario_id: verificar._id, categoria_id: categoria, num_categoria: numero, status: 'A' })
                }
                // Save the document
                result.save(function (error) {
                    if (!error) {
                        return res.status(200).send({ message: 'Area registrada com sucesso ! ' })
                    } else {
                        return sendErrorsFromDB(res, error);
                    }
                });
            } else {
                return sendErrorsFromDB(res, error)
            }
        });
    } else {
        return res.status(400).send({ error: ['unauthorized'] })
    }


}
 */