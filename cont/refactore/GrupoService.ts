import { strToBool, validar } from '../config/validateAcess';
import {sendErrorsFromDB} from '../config/utils';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import Grupo from '../models/Treino/grupo';
import GrupoFuncionario from '../models/Funcionario/GrupoFuncionario';
import GrupoFuncionarioAtividades from '../models/Funcionario/servicos/GrupoFuncionarioAtividades';

export const registrarGrupo = (req, res, next) => {
    const grupo = req.body.grupo || ''
    var exercicio = req.body.exercicio || []
    const semana = req.body.semana || ''
    const observacao = req.body.observacao || ''


    var validate = validar(req, [FUNCIONARIO]);

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada'],

        })
    }


    const saveGrupo = new Grupo({
        instrutor: validate.id,
        exercicios: JSON.parse(exercicio),
        grupo: grupo,
        dia_semana: semana,
        observacao: observacao
    });


    saveGrupo.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error)
        } else {
            return res.status(200).send({ message: ['Grupo registrado com sucesso'] })
        }
    });
}

export const listarGrupo = (req, res) => {

    var validate = validar(req, [FUNCIONARIO]);

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']

        })
    }

    Grupo.find({
        status: 'A',
        instrutor: validate.id
    })
        .populate('instrutor', ['_id', 'nome', 'sobrenome', 'email'])
        .populate('dia_semana')
        .populate({
            path: 'exercicios.exercicio',
            populate: { path: 'membro' }
        })
        .exec((error, result) => {
            if (error) {
                res.status(500).json({
                    errors: [error]
                })
            }
            res.json(result)
        });

}