
import * as pagarme from 'pagarme';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import {sendErrorsFromDB} from '../config/utils';
import Pagamento from '../models/Pagamento/Pagamento'
import { strToBool, validar } from '../config/validateAcess';

export const meusPagamento = (req, res) => {

    var validate = validar(req, [ALUNO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

    Pagamento.find({ aluno: validate.detalhe.aluno._id })
        .populate({
            path: 'aluno',
            populate: ({
                path: 'user',
                select: ['nome', 'sobrenome', 'email'],
                populate: ({ path: 'tp_user' })

            })
        })
        .populate({
            path: 'funcionarioAtv',
            populate: ({
                path: 'funcionario_id',
                populate: ({
                    path: 'user_id',
                    select: ['nome', 'sobrenome', 'email']
                })
            })
        })
        .populate('funcionarioGrupoAtv')
        .exec((error, sucesso) => {
            if (error) {
                return sendErrorsFromDB(res, error);
            }
            res.json(sucesso)
        })



}


export const realizarPagamento = (req, res) => {

    const funcionarioAtv = req.body.funcionarioAtv || null
    const funcionarioGrupoAtv = req.body.funcionarioGrupoAtv || null
    var validate = validar(req, [ALUNO]);

    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

    new Pagamento({
        aluno: validate.detalhe.aluno._id,
        retornoPagamento: '',
        funcionarioAtv,
        funcionarioGrupoAtv

    }).save((error) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        }

        return res.status(200).send({ message: ['Pagamento realizado com sucesso ! '] })
    })

}

export const pagamento = (req, res) => {
    pagarme.client.connect({ api_key: process.env.KEY_SERVER.trim() })
    .then(client => client.plans.create({
        amount: 150,
        days: 30,
        name: 'Pagamento consultoria com consultor Hugo',
        payment_methods: [  'credit_card']
    }))
    .then(plan =>
        
        
     {
        return res.status(400).send({
            plan
        })
     }
    )
}