import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO, MESSAGE_02, MESSAGE_01, MESSAGE_03 } from '../config/constants';
import {sendErrorsFromDB} from '../config/utils'
import { strToBool, validar } from '../config/validateAcess';
import Plano from '../models/Treino/plano';
import PlanoAcademia from '../models/Treino/PlanoAcademia';
import PlanoAdesao from '../models/plano/planoAdesao';
import PlanoVigencia from '../models/plano/planoVigencia';


/**
 * 
 * @param req Função que registra o plano de exercicio do aluno na academia
 * @param res 
 */
export const registrarPlanoAlunoEmpresa = (req, res) => {
    const aluno = req.body.aluno || ''
    const grupo = req.body.grupo || ''
    const empresa = req.body.empresa || ''
    const observacao = req.body.observacao || ''
    var grupoArrayFinal = new Array();

    var validate = validar(req, [FUNCIONARIO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    var grupoArray = JSON.parse(grupo.toString())
    grupoArray.forEach(function (rm) {
        grupoArrayFinal.push(rm.id)
    });
    const savePlano = new PlanoAcademia({
        empresa,
        aluno: aluno,
        instrutor: validate.id,
        grupo: grupoArrayFinal,
        observacao
    });
    savePlano.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error)
        } else {
            return res.status(200).send({
                message: ['Plano registrado com sucesso']
            })
        }
    })
}

export const listarPlanoAlunoEmpresa = (req, res) => {

    var validate = validar(req, [FUNCIONARIO, EMPRESA, ALUNO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    PlanoAcademia.find({}, (error, plano) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        }
        res.status(200).send(plano)
    })
}

export const registrarPlano = (req, res, next) => {
    const aluno = req.body.aluno || ''
    const grupo = req.body.grupo || ''
    const observacao = req.body.observacao || ''

    var grupoArrayFinal = new Array();
    var validate = validar(req, [FUNCIONARIO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    var grupoArray = JSON.parse(grupo.toString())
    grupoArray.forEach(function (rm) {
        grupoArrayFinal.push(rm.id)
    });
    // Fetch the user by id                                        
    const savePlano = new Plano({
        aluno: aluno,
        instrutor: validate.id,
        grupo: grupoArrayFinal,
        observacao
    });
    savePlano.save(error => {
        if (error) {
            return sendErrorsFromDB(res, error)
        } else {
            return res.status(200).send({
                message: ['Plano registrado com sucesso']
            })
        }
    })
}

export const listarPlano = (req, res) => {

    var validate = validar(req, [FUNCIONARIO, ALUNO]);
    if (!strToBool(validate.status)) {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
    Plano.find({}, (error, plano) => {
        if (error) {
            return sendErrorsFromDB(res, error)
        }
        res.status(200).send(plano)
    })
}


export const registrarPlanoAdesao = (req, res, next) => {
    const descricao = req.body.descricao || ''
    const valor = req.body.valor || ''
    const dias = req.body.dias || ''

    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        const savePlanoAdesao = new PlanoAdesao({
            descricao,
            valor,
            dias
        })
        savePlanoAdesao.save(error => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else {
                return res.status(200).send({
                    message: 'Plano registrado com sucesso !'
                })
            }
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}

export const listarPlanoAdesao = (req, res, next) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        PlanoAdesao.find({}, function (err, result) {
            if (err) {
                return sendErrorsFromDB(res, err)
            } else {
                return res.status(200).send(result)
            }
        });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}

export const registrarPlanoVigencia = (req, res, next) => {

    const data_inicio = req.body.data_inicio || ''
    const data_fim = req.body.data_fim || ''
    const empresa = req.body.empresa || ''
    const pacote = req.body.pacote || ''

    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        PlanoVigencia.findOne({
            empresa
        }, (error, exPlano) => {
            if (error) {
                return sendErrorsFromDB(res, error)
            } else if (exPlano) {

                return res.status(400).send({
                    message: 'Existe um plano cadastrado nesta empresa !'
                })
            } else {
                const savePlanoVigencia = new PlanoVigencia({
                    data_inicio,
                    data_fim,
                    empresa,
                    pacote
                })
                savePlanoVigencia.save(error => {
                    if (error) {
                        return sendErrorsFromDB(res, error)
                    } else {
                        return res.status(200).send({
                            message: 'Plano vigencia registrado com sucesso !'
                        })
                    }
                })
            }
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}

export const listarPlanoVigencia = (req, res) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        PlanoVigencia.find({})
            .populate('empresa')
            .populate('pacote ')

            .exec((error, result) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                } else {
                    res.json(result)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}