import * as  _ from 'lodash';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO } from '../config/constants';
import { strToBool, validar } from '../config/validateAcess';
import {sendErrorsFromDB} from '../config/utils';
import Tipo from '../models/User/Tipo';
import User from '../models/User/User';
import Aluno from '../models/Aluno/Aluno';
import Pagamento from '../models/Pagamento/Pagamento';
import EmpresaAluno from '../models/Empresa/EmpresaAluno';
import Funcionario from '../models/Funcionario/Funcionario';
import AlunoFuncionario from '../models/Funcionario/AlunoFuncionario';
import EmpresaFuncionario from '../models/Empresa/EmpresaFuncionario'
import Empresa from '../models/Empresa/Empresa';
import Semana from '../models/Treino/Semana';




/**
 * Função para registrar o TpUser de usuario no sistema
 * @param {tp_user} req 
 * @param {error, message} res 
 */
export const registrarTpUserUsuario = async (req, res, next) => {

    var tipo = req.body.tipo || null
    var validate = await validar(req, [ADMIN]);
 
    if (validate.status) {
        Tipo.findOne({
                tipo: tipo
            }, (error, user) => {
                if (error) {
                    return sendErrorsFromDB(res, error)
                } else if (user) {
                    return res.status(400).send({
                        error: ['Tipo de usuário já cadastrado']
                    })
                } else {
                    const saveUser = new Tipo({
                        tipo: tipo
                    });
                    saveUser.save(error => {
                        if (error) {
                            return sendErrorsFromDB(res, error)
                        } else {
                            return res.status(200).send({
                                message: ['Tipo de usuario registrado com sucesso']
                            })
                        }
                    })
                }
            })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }  
}
/**
 * Função para listar os TpUser de usuarios existentes no sistema
 * @param {nenhum} req 
 * @param {*} res 
 */
export const listarTpUser = (req, res) => {
    Tipo.find({}, function (err, result) {
        if (err) {
            return sendErrorsFromDB(res, err)
        } else {
            return res.status(200).send(result)
        }
    });
}



/***
 * Função para listar as empresas
 * * @param {} req 
 * @param {*} res 
 */
export const listarEmpresas = (req, res, next) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        Empresa.find({}, function (err, result) {
            if (err) {
                return sendErrorsFromDB(res, err)
            } else {
                return res.status(200).send(result)
            }
        });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }
}
export const listarSemana = (req, res) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        Semana.find({})
            .exec((error, result) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                } else {
                    res.json(result)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}

export const registrarSemana = (req, res) => {
    const semana = req.body.semana || ''
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        Semana.findOne({
            semana
        }, (error, sucesso) => {
            if (error) {
                return sendErrorsFromDB(res, error);
            } else if (sucesso) {
                return res.status(400).send({
                    message: 'Dia da semana ja registrado! '
                })
            } else {
                const saveSemana = new Semana({ semana })
                saveSemana.save((error) => {
                    if (error) {
                        return sendErrorsFromDB(res, error);
                    }
                    return res.status(200).send({
                        message: 'Dia da semana registrado com sucesso! '
                    })
                })

            }
        })
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }


}

