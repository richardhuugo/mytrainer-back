
import { ALUNO, ADMIN, EMPRESA, FUNCIONARIO, MESSAGE_02, MESSAGE_01, MESSAGE_03 } from '../config/constants';
import {sendErrorsFromDB} from '../config/utils'
import { strToBool, validar } from '../config/validateAcess';
import Categoria from '../models/Funcionario/Categoria';

export const registrarCategoria = (req, res) => {
    const categoria = req.body.categoria || ''

    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        var update = {
            categoria
        }
        Categoria.findOneAndUpdate({
            categoria
        }, update, {
                upsert: true,
                new: true,
                setDefaultsOnInsert: true
            }, function (error, result) {
                if (!error) {
                    // If the document doesn't exist
                    if (!result) {
                        // Create it
                        result = new Categoria({
                            categoria
                        })
                    }
                    // Save the document
                    result.save(function (error) {
                        if (!error) {
                            return res.status(200).send({
                                message: 'Categoria registrada com sucesso ! '
                            })
                        } else {
                            return sendErrorsFromDB(res, error);
                        }
                    });
                } else {
                    return sendErrorsFromDB(res, error)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}

export const listarCategoria = (req, res) => {
    var validate = validar(req, [ADMIN]);
    if (validate.status) {
        Categoria.find({})
            .exec((error, result) => {
                if (error) {
                    res.status(500).json({
                        errors: [error]
                    })
                } else {
                    res.json(result)
                }
            });
    } else {
        return res.status(400).send({
            error: ['Você não tem permissão para efetuar essa chamada']
        })
    }

}
