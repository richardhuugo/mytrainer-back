import Membro from "../models/Membro";
import Exercicio from "../models/Exercicio";
import Modalidades from "../models/Modalidades";


export const registrarExercicio = async (req, res, next) => {
    const {exercicio, membro_id, modalidade_id} = req.body
 
    // cria novo exercicio
    const newExercicio = new Exercicio({
        exercicio,
        membro:membro_id,
        modalidades:modalidade_id
    })
    newExercicio.save();

    return res.status(200).send({message:"Exercicio registrado com sucesso!"})
}
 

export const deletarExercicio = async (req, res, next) => {
    const {exercicio_id, status} = req.body
    const exercicio = await Exercicio.findOneAndUpdate({_id:exercicio_id}, {status:status}, {upsert:true});   

    if(exercicio === null)
        return res.status(400).send({message:'O Exercicio informado não se encontra registrado'})

    return res.status(200).send({message:'Ação de excluir executada com sucesso!'})
}
// MODALIDADES

export const registrarModalidades = (req, res, next) => {
    const {modalidade,descricao} = req.body

    const registrarModalidade = new Modalidades({
        modalidade,
        descricao
    });

    registrarModalidade.save();

    return res.status(200).send({message:"Modalidade registrada com sucesso!"})
}

export const listarModalidades = async (req, res, next) => {
  
    const modalidades = Modalidades.find();
    
    return res.status(200).send(modalidades)
}

// MEMBROS
export const createMembro = async (req, res, next) => {
    const {membro} = req.body

    const registrarMembro = new Membro({
        membro
    });

    registrarMembro.save();

    return res.status(200).send({message:"Membro registrado com sucesso!"})

}
 
export const listarMembro = async (req, res, next) => {

    const membro = await Membro.find()

    return res.status(200).send(membro)
}
