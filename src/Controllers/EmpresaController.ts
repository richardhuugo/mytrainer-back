import EmpresaFuncionario from "../models/EmpresaFuncionario";

import { verifyNull, findFuncionarioEmpresa, findUser, findTipo, findAlunoEmpresa, findUserById, verificarPerfil } from './UtilController';
import Usuario from "../models/Usuarios/Usuario";

import EmpresaAluno from "../models/EmpresaAluno";
import * as bcrypt from 'bcrypt';
import { ALUNO, FUNCIONARIO } from "../config/constants";
import { gerarPassword } from "../config/utils";
import PlanoEmpresaAluno from "../models/PlanoEmpresaAluno";
import { resolve } from "bluebird";
import PacoteAluno from "../models/PacoteAluno";
import UsuarioPerfis from "../models/Usuarios/UsuarioPerfis";

// ======================================= FUNCIONARIO ============================================================
export const registrarFuncionario = async (req, res, next) => {
    const {user_id, nome, sobrenome, email,documento} = req.body
    const {empresa,status,tipo} = req.decoded;
   
    if(user_id != "" && user_id != undefined){
        const tipo = await findTipo(FUNCIONARIO)  
        const usuario = await findUserById(user_id)      
        const funcionario = await UsuarioPerfis.findOne({user:usuario._id,tipo:tipo._id})
        if(funcionario== null)
            return res.status(400).json({message:'Funcionario não encontrado!'})

        const funcEmpresa = await findFuncionarioEmpresa(empresa._id, usuario._id);
        if(funcEmpresa != null)
            return res.status(400).send({message:'Funcionario já se encontra registrado nessa empresa.'})
        
        return await funcionarioExistente(usuario, empresa, res)   
    }
 
    return await novoFuncionario(nome, sobrenome, email,documento, empresa, res);
}
export const listarFuncionario = async (req, res, next) => {
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const empresaFuncionario =  await 
     EmpresaFuncionario
    .find({empresa_id:empresa._id})    
    .populate({  path: 'user',   
        select:['_id','nome','sobrenome','email']
    }).exec()
    

    return res.json(empresaFuncionario)
}
export const buscarFuncionario = async (req, res, next) => {
    var {documento} = req.body;
        const usuario = await findUser(documento)
        if(usuario === null)
            return res.status(400).send({message:'Usuário não encontrado'})
            
        const tipo = await findTipo(FUNCIONARIO)
        const funcionario = await verificarPerfil(usuario._id, tipo._id);        
        if(!funcionario)
            return res.status(400).send({message:"Funcionario não encontrado!"})

    return res.status(200).send({funcionario})
}
export const updateFuncionario = async (req, res, next) => {
    const {user_id, statusAtualizar} = req.body
    const {empresa,status,tipo} = req.decoded;
    
    const empresaFuncionario = await EmpresaFuncionario.findOneAndUpdate({user:user_id, empresa_id:empresa._id}, {status:statusAtualizar}, {upsert:true});   

    if(empresaFuncionario === null)
        return res.status(400).send({message:'O funcionario informado não se encontra registrado nesta empresa!'})

    return res.status(200).send({message:'Ação executada com sucesso!'})
    
}
// ========================================== ALUNO ======================================================================

export const capturarInfoAluno = async (req, res, next) => {
    const {cpf, type} = req.query;
   let teste = cpf;   
    const usuario = await Usuario.findOne({documento: teste[0]}).exec()
    if(usuario ==null)
        return res.json({message:'Usuario não existente', usuario:false});

    if(type ==0){
        const tipo = await findTipo(FUNCIONARIO);
        const aluno = await verificarPerfil(usuario._id, tipo._id)

        if(aluno ==null)
            return res.json({message:'Aluno não existe', usuario:true, aluno:false, data:usuario})

        return res.json({usuario:true, aluno:true, data:usuario});
    }else if(type==1){
        const tipo = await findTipo(FUNCIONARIO);
        const funcionario = await verificarPerfil(usuario._id, tipo._id)       

        if(funcionario == null)
            return res.json({message:'Funcionario não existe', usuario:true, funcionario:false, data:usuario})

        return res.json({usuario:true, funcionario:true, data:usuario})     
    } else {
        return res.json({message:'Tipo de verificação incorreta.',usuario:false})     
    }   
          
}


export const registrarAlunoPlano = async (req, res, next) => {
    const {plano_id, aluno_id, nome, sobrenome, email, documento} = req.body;


}

// desabilitar essa funcao
export const registrarAluno = async (req, res, next) => {
    const {user_id, nome, sobrenome, email,documento} = req.body
    const {empresa,status,tipo} = req.decoded;
    if(user_id != "" && user_id != undefined){
        const usuario = await findUserById(user_id)
        if(usuario == null)
            return res.status(400).send({message:'Usuário não encontrado.'})

        const alunoEmpresa = await findAlunoEmpresa(empresa._id,usuario._id);
        if(alunoEmpresa != null)
            return res.status(400).send({message:'Aluno já se encontra registrado nessa empresa.'})
        
                
         return await alunoExistente(usuario, empresa, res)   
    }
    const aluno = await findUser(documento);
    if(aluno != null)
        return res.status(400).json({message:'O aluno já possui cadastro no sistema!'})

    return await novoAluno(nome, sobrenome, email,documento, empresa, res);
    
}
export const deletarAluno = async (req, res, next) => {
    const {user_id,statusAtualizar} = req.body
    const {empresa,status,tipo} = req.decoded;
    
    const alunoEmpresa = await EmpresaAluno.findOneAndUpdate({user:user_id, empresa_id:empresa.id}, {status:statusAtualizar}, {upsert:true});   

    if(alunoEmpresa === null)
        return res.status(400).send({message:'O aluno informado não se encontra registrado nesta empresa!'})

    return res.status(200).send({message:'Ação executada com sucesso!'})
        
}
export const listarAluno = async (req, res, next) => {
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const empresaAluno =  await 
     EmpresaAluno
    .find({empresa_id:empresa._id})
    .populate({ path: 'user',   
         select:['_id','nome','sobrenome','email']
    }).exec()
    
    return res.json(empresaAluno)
}
// =================================== PLANOS ===========================================================================
export const registrarPlanoEmpresaAluno = (req, res, next) => {
    const {descricao, valor, periodo} = req.body;
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;

    const plano = new PlanoEmpresaAluno({
        empresa_id:empresa._id,
        descricao, 
        valor, 
        periodo
    });
    plano.save(error => {
        if(error)
            return res.status(400).send({message:'Error ao registrar plano: '+error})

        return res.status(200).json({message:'Plano registrado com sucesso!'})
    });

    
}
export const excluirPlanoEmpresaAluno = async (req, res, next) => {
    const {plano_id, statusPlano} = req.body
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    
    const planoEmpresaAluno = await PlanoEmpresaAluno.findOneAndUpdate({_id:plano_id, empresa_id:empresa._id}, {status:statusPlano}, {upsert:true});   

    if(planoEmpresaAluno === null)
        return res.status(400).send({message:'O plano informado não se encontra registrado nesta empresa!'})

    return res.status(200).send({message:'Ação de excluir executada com sucesso!'})

}
export const listarPlanosEmpresaAluno = async (req, res, next) => {
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const lista = await PlanoEmpresaAluno.find({empresa_id:empresa._id, status:'A'});
    return res.status(200).json(lista)
}
export const capturarInfoEmpresa = async (req, res, next) => {
    const {cpnj} = req.query

    
}
export const exporPlanoEmpresa = async (req, res, next) => {
    const {empresa_id} = req.query

    const lista = await PlanoEmpresaAluno.find({empresa_id:empresa_id, status:'A'});
    return res.json(lista)
}
// =================================== PACOTE ======================================================================
// desabilitar essa funcao
export const registrarPacoteAluno = async (req, res, next) => {
    const {user_id,plano_id} = req.body;
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const aluno = await EmpresaAluno.findOne({empresa_id:empresa._id, user:user_id})
    const plano = await PlanoEmpresaAluno.findOne({_id:plano_id,empresa_id:empresa._id});
    if(aluno==null )
        return res.status(400).json({message:'Aluno não se encontra registrar nesta empresa!'})
    
    if(plano==null )
        return res.status(400).json({message:'Plano não se encontra registrar nesta empresa!'})
    
    const registrarPacoteAluno = new PacoteAluno({
        plano:plano._id,
        user:user_id
    }); 
    registrarPacoteAluno.save();

    return res.status(400).json({message:'Adesão do pacote realizada com sucesso!'})
}
export const excluirPacoteAluno = async (req, res, next) => {
    const {user_id,plano_id} = req.body;
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const aluno = await EmpresaAluno.findOne({empresa_id:empresa._id, user:user_id})
    const plano = await PlanoEmpresaAluno.findOne({_id:plano_id,empresa_id:empresa._id});
    
    if(aluno==null )
        return res.status(400).json({message:'Aluno não se encontra registrar nesta empresa!'})
    
    if(plano==null )
        return res.status(400).json({message:'Plano não se encontra registrar nesta empresa!'})
    
    
    const pacoteAlunoEmpresa = await PacoteAluno.findOneAndUpdate({plano:plano._id,user:user_id}, {status:'I'}, {upsert:true});   

    if(pacoteAlunoEmpresa === null)
        return res.status(400).send({message:'Não conseguimos excluir o pacote do aluno selecionado!'})
    
    return res.status(200).send({message:'Ação de excluir executada com sucesso!'})
}
export const listarPacotesAlunos = async (req, res, next) => {
    const {empresa,status,tipo,nome, sobrenome, email} = req.decoded;
    const planos = await PlanoEmpresaAluno.find({empresa_id:empresa._id});
    var plan = [];
    if(planos != null){
        planos.map((val) => {
            plan.push(val._id)
        })
        
    }
    const pacotes = await PacoteAluno.
    find({plano:{$all : plan }})
    .populate({
        path:'user',
        select:['_id','nome','sobrenome','email']
    }).exec();

    return res.json(pacotes)
}


const novoAluno = async (nome, sobrenome, email,documento, empresa, res ) => {
    const salt = bcrypt.genSaltSync()
    const senha = gerarPassword();
    const passwordHash = bcrypt.hashSync('12345', salt)

    const tipo = await findTipo(ALUNO);
    const usuario = new Usuario({
        nome,
        sobrenome,
        email,
        senha:passwordHash,
        documento,
    });
    
    await new UsuarioPerfis({
        user:usuario._id,
        tipo:tipo._id
      }).save((error)=>{
        if (error)
          return res.status(400).send(error)
      });
    const empresaAluno = new EmpresaAluno({
        empresa_id:empresa._id,
        user:usuario._id
    });

    await usuario.save();
    
    await empresaAluno.save();

    return res.json({message:'Aluno registrado com sucesso!'})
}

const alunoExistente = async (aluno, empresa, res) => {
    await new EmpresaAluno({
        empresa_id:empresa._id,
        user:aluno._id
    }).save(error => {
        if(error)
            return res.status(400).json({message:'Falha ao cadastrar aluno!'})
    });

    return res.status(200).json({message:'Aluno registrado com sucesso!'})
}

const novoFuncionario = async (nome, sobrenome, email, documento, empresa, res ) => {
    const salt = bcrypt.genSaltSync()
    const senha = gerarPassword();
    const passwordHash = bcrypt.hashSync('12345', salt)

    const tipo = await findTipo(FUNCIONARIO);
    const usuario = new Usuario({
        nome,
        sobrenome,
        email,
        senha:passwordHash,
        documento       
    });
   
   await new UsuarioPerfis({
        user:usuario._id,
        tipo:tipo._id
      }).save((error)=>{
        if (error)
          return res.status(400).send(error)
      });
      
    const empresaFuncionario = new EmpresaFuncionario({
        empresa_id:empresa._id,
        user:usuario._id
    });

    await usuario.save();
    await empresaFuncionario.save();

    return res.json({message:'Funcionario registrado com sucesso!'})
}
const funcionarioExistente = async (funcionario, empresa, res) => {
    await new EmpresaFuncionario({
        empresa_id:empresa._id,
        user:funcionario._id
    }).save(error => {
        if(error)
            return res.status(400).json({message:'Falha ao cadastrar funcionario!'})
    });

    return res.status(200).json({message:'Funcionario registrado com sucesso!'})
}


 