import { findUser, findUserEmail, verificarPerfis } from './UtilController';
import * as bcrypt from 'bcrypt';
import { ADMIN, ALUNO, EMPRESA, FUNCIONARIO } from "../config/constants";

import * as jwt from 'jsonwebtoken';

import EmpresaFuncionario from "../models/EmpresaFuncionario";
import EmpresaAluno from "../models/EmpresaAluno";
import AdministradorEmpresa from "../models/Usuarios/AdministradorEmpresa";

export const login = async (req, res, next) => {
    const {email, senha} = req.body

    const user = await findUserEmail(email)
    
    if(user == null) 
        return res.status(500).json({message:'Usuario ou senha inválidos'})
    
    if(!bcrypt.compareSync(senha, user.senha))
        return res.status(500).json({message:'Usuario ou senha inválidos'})

    let toObject = user.toObject();        
    
    var toObjectAluno = await aluno( user);
    var toObjectEmpresa = await empresa(user);
    var toObjectFuncionario =await funcionario(user, toObject)    ;
    toObject.aluno = toObjectAluno
    toObject.empresa = toObjectEmpresa
    toObject.funcionario = toObjectFuncionario
    toObject.perfis = await verificarPerfis([
        toObject.aluno.perfil ,
        toObject.empresa.perfil,
        toObject.funcionario.perfil
     ]);

     const token = jwt.sign(toObject, process.env.KEY_SERVER.trim(), {
        expiresIn: "8h"
     })
    
    
    return res.status(200).send({ nome: user.nome, tp: toObject.perfis, token})
}
 
const aluno = async (user) => {

    const empresa = await EmpresaAluno.find({user_id:user._id, status:'A'}).populate('empresa_id').exec();
    if(empresa == null )
    return  {}

    return {
        empresa:empresa,
        perfil:ALUNO
    };
}

const empresa = async (user) => {
    const empresa = await AdministradorEmpresa.find({ user: user._id }).populate('empresa_id').exec()    
    if(empresa.length == 0)
        return {};
        
    return {empresa, perfil:EMPRESA};
}
const funcionario = async ( user,toObject) => {
    
    const empresa = await EmpresaFuncionario.find({ status: 'A', user_id: user._id }).populate('empresa_id').exec();   
    if(empresa == null)
         return {};  

    return {funcionario:funcionario, empresa:empresa, perfil:FUNCIONARIO} ;
       
}