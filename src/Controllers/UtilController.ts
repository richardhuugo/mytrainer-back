import TipoUsuario from "../models/Usuarios/TipoUsuario";
import Usuario from "../models/Usuarios/Usuario";

import EmpresaFuncionario from "../models/EmpresaFuncionario";
import EmpresaAluno from "../models/EmpresaAluno";

import { ALUNO, FUNCIONARIO, EMPRESA } from "../config/constants";
import UsuarioPerfis from "../models/Usuarios/UsuarioPerfis";
import Cidade from "../models/Cidade";

export const findTipo = async (tipo) => {
    return await TipoUsuario.findOne({tipo});
}
export const findUserEmail = async (email) => {
    return await Usuario.findOne({ email }).exec()
}
export const verificarPerfis = async (perfis) => {
    let perfisArray = [ALUNO,FUNCIONARIO,EMPRESA];
    let data = [];
    await perfis.map(async perfil => {
        if(perfisArray.includes(perfil))
            data.push(perfil)

    })

    return data
}
export const findCidadeById = async (cidade) => {
    return await Cidade.findOne({cidade})
}
export const findUser = async (documento) => {
    return await Usuario.findOne({ documento }).exec()
}
export const findUserById = async (id) => {
    return await Usuario.findOne({ _id:id }).exec()
}
export const verificarPerfil = async (usuario_id, tipo_id) => {
    return await UsuarioPerfis.findOne({user:usuario_id, tipo:tipo_id})
}

export const findFuncionarioEmpresa = async (empresa_id, user) => {
    return   await EmpresaFuncionario.findOne({ $and: [{empresa_id:empresa_id},{user} ]})  
}
export const findAlunoEmpresa = async (empresa_id, user) => {
    return   await EmpresaAluno.findOne({ $and: [{empresa_id:empresa_id},{user} ]})  
}
export const verifyNull = async (val, res, text) => {
    if(val === null)
        return res.status(400).json({message:text})

    return val
}