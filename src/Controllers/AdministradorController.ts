import TipoUsuario from "../models/Usuarios/TipoUsuario";

export const registrarTipo = (req, res, next) => {
    const tipo = req.body.tipo || ''
    new TipoUsuario({tipo}).save((error) => {
        if(error){
            return res.status(400).send(error)
        }
        return res.status(200).send('Cadastrado com sucesso');
    })
}