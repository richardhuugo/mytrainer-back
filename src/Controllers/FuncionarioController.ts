import EmpresaFuncionario from "../models/EmpresaFuncionario";
import EmpresaAluno from "../models/EmpresaAluno";


export const listarAlunos = async (req, res, next) => {
    const {funcionario} = req.body;
    const listarEmpresas = await EmpresaFuncionario.find({user:funcionario._id});
    const empresas =[];
    if(listarEmpresas ===null)
        return res.status(200).send({message:'Ops você não está vinculado a nenhuma empresa!'})

    listarEmpresas.map(val => {
        empresas.push(val.empresa_id)
    })    

    const alunos = await EmpresaAluno.
    find({empresa_id:{$all : empresas }})
    .populate({
        path: 'user',
        select:['_id','nome','sobrenome','email']
    }) .exec();

    return res.json(alunos)
}
