import Usuario from "../models/Usuarios/Usuario";
import { findTipo, findUser,  findUserEmail, verificarPerfil, findCidadeById } from './UtilController';
import * as bcrypt from 'bcrypt';
import { ALUNO, FUNCIONARIO, EMPRESA } from "../config/constants";
import Empresa from "../models/Empresa";
import EmpresaFuncionario from "../models/EmpresaFuncionario";
import AdministradorEmpresa from "../models/Usuarios/AdministradorEmpresa";
import Endereco from "../models/Endereco";
import UsuarioPerfis from "../models/Usuarios/UsuarioPerfis";

export const registrarAluno = async (req, res, next) => {
    const {nome, sobrenome, email,documento,  senha} = req.body;

    const salt = bcrypt.genSaltSync()
    const passwordHash = bcrypt.hashSync(senha, salt)

    const aluno = await findTipo(ALUNO);
    const usuarioFind = await findUser(documento);

    if(usuarioFind !=null){      
         const verificar = await verificarPerfil(usuarioFind._id,aluno._id)
         if(verificar !==null){
          return res.status(400).send({message:"Ops, cadastro existente. Tente recuperar a senha!"})
         } 
          
          new UsuarioPerfis({
            user:usuarioFind._id,
            tipo:aluno._id
          }).save((error)=>{
            if (error)
              return res.status(400).send(error)
          });

          return res.status(200).send({message:'Usuario criado com sucesso!'})

         
    }else{
      const usuario = new Usuario({
        nome,
        sobrenome,
        email,
        senha:passwordHash,
        documento  
      });
      new UsuarioPerfis({
        user:usuario._id,
        tipo:aluno._id
      }).save((error)=>{
        if (error)
          return res.status(400).send(error)
      });
      usuario.save((error)=>{
        if (error)
          return res.status(400).send(error)
      });
      

      return res.status(200).send({message:'Usuario criado com sucesso!'})

    }
 
}

export const registrarFuncionario = async (req, res, next) => {
  const {nome, sobrenome, email, senha,documento} = req.body;

  const salt = bcrypt.genSaltSync()
  const passwordHash = bcrypt.hashSync(senha, salt)

  const funcionario = await findTipo(FUNCIONARIO);
  const funcionarioFind = await findUser(documento);

  if(funcionarioFind !== null){    
    const func = await  verificarPerfil(funcionarioFind._id,funcionario._id)    
    if(func !== null){
      return res.status(400).send({message:'Ops, usuario já registrado, tente recuperar a senha'})
    }

    new UsuarioPerfis({
      user:funcionarioFind._id,
      tipo:funcionario._id
    }).save((error)=>{
      if (error)
        return res.status(400).send(error)
    });
    
    return res.status(200).send({message:'Usuario criado com sucesso!'})
  }else{
    const usuario = new Usuario({
      nome,
      sobrenome,
      email,
      senha:passwordHash,
      documento
    });

    usuario.save((error)=>{
      if (error)
        return res.status(400).send(error)
    });
    new UsuarioPerfis({
      user:usuario._id,
      tipo:funcionario._id
    }).save((error)=>{
      if (error)
        return res.status(400).send(error)
    });
    
  
    return res.status(200).send({message:'Usuario criado com sucesso!'})
  }
  
}

export const registrarEmpresa = async (req, res, next) => {
 
  const {email, senha, nm_fantasia, razao_social, cnpj, cep, cidade, bairro} = req.body;

  const salt = bcrypt.genSaltSync()
  const passwordHash = bcrypt.hashSync(senha, salt)

  const empresaTipo = await findTipo(EMPRESA);
  const usuarioFind = await findUserEmail(email);
  let user = null;
  if(usuarioFind !== null){
    user =usuarioFind;
  }else{
      user = new Usuario({
      nome:"Administrador",
      sobrenome:razao_social,
      email,
      senha:passwordHash,
      documento:cnpj   
    });
  }
  const cidadeFind = await findCidadeById(cidade)
  if(cidadeFind == null)
    return res.status(400).json({message:'Cidade não encontrada'})
 
  const enderecoEmpresa = new Endereco({
    cep,
    bairro,
    cidade_id:cidadeFind._id
  });
  const empresa = new Empresa({
    nm_fantasia,
    razao_social,
    cnpj,    
    endereco:enderecoEmpresa._id,  
  });
  const verificar = await verificarPerfil(user._id,empresaTipo._id)
   if(verificar == null){
    new UsuarioPerfis({
      user:user._id,
      tipo:empresaTipo._id
    }).save((error)=>{
      if (error)
        return res.status(400).send(error)
    });
   }
 
  const administradorEmpresa = new AdministradorEmpresa({
    empresa_id:empresa._id ,
    user:user._id
  });
  
  user.save(  );  
  enderecoEmpresa.save(); 
  empresa.save( );
  administradorEmpresa.save( );

  // criar template de string com mensagem de retorno
  return res.status(200).send({message:'Criado com sucesso!'})
}
