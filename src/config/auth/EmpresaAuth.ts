import {  EMPRESA } from "../constants";
const jwt = require('jsonwebtoken')

const EmpresaAuth =  (req, res, next) => {

        if (req.method === 'OPTIONS') {
            next()
        } else {
            const token =  req.headers['authorization']
            if (!token) 
                return res.status(500).send({ errors: ['No token provided.'] })
            
            if(token.search("Bearer") !=0 )
                return res.status(403).json({ errors: ['No token provided.'] })
                
            const divid = token.split(" ");
            
            jwt.verify(divid[1],  process.env.KEY_SERVER.trim(), function (err, decoded) {
                if (err) {
                    return res.status(403).send({
                        errors: ['Failed to authenticate token.'],
                        invalid:true
                    })
                } else {                     
                    if( !decoded.perfis.includes(EMPRESA) )
                        return res.status(403).send({ errors: ['Failed to authenticate token.'] , invalid:true})
                                    
                    req.decoded = decoded
                    next()
                }
            })
        }
       
   
}

export default EmpresaAuth;