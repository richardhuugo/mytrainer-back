const ALUNO = "Aluno";
const EMPRESA= "Empresa";
const ADMIN= "Administrador";
const FUNCIONARIO= "Funcionario";
const MESSAGE_01  ='O email informado já está registrado no sistema.';
const MESSAGE_02  ='Aluno registrado com sucesso! ';
const MESSAGE_03  ='Tipo de usuario não encontrado.';
const MESSAGE_04  ='O email informado já está registrado no sistema.';
const REGISTRAR_TIPO_USER = 'registrar-tipo-usuario';
const REGISTRAR_MODALIDADE = 'registrar-modalidade';
const REGISTRAR_MEMBRO = 'registrar-membro';
const REGISTRAR_EXERCICIO = 'registrar-exercicio';
const REGISTRAR_GRUPO  ='registrar-grupo';
//empresa
const REGISTRAR_FUNCIONARIO_EMPRESA = "REGISTRAR_FUNCIONARIO_EMPRESA";
const LISTAR_FUNCIONARIO_EMPRESA = "LISTAR_FUNCIONARIO_EMPRESA";
const DELETAR_FUNCIONARIO_EMPRESA = "DELETAR_FUNCIONARIO_EMPRESA";
const REGISTRAR_ALUNO_EMPRESA = "REGISTRAR_ALUNO_EMPRESA";
const DELETAR_ALUNO_EMPRESA = "DELETAR_ALUNO_EMPRESA";
const LISTAR_ALUNO_EMPRESA = "LISTAR_ALUNO_EMPRESA";
export  {
    REGISTRAR_GRUPO,
    REGISTRAR_EXERCICIO,
    REGISTRAR_MODALIDADE,
    REGISTRAR_MEMBRO,
    ALUNO, 
    ADMIN, 
    EMPRESA, 
    FUNCIONARIO,
    MESSAGE_01,
    MESSAGE_02,
    MESSAGE_03,
    MESSAGE_04,
    REGISTRAR_TIPO_USER
}