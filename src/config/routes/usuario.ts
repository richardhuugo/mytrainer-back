import {validator, verify}  from '../../utils/Validator'
import {registrarAluno, registrarFuncionario, registrarEmpresa} from '../../Controllers/UsuarioController';
import { login } from '../../Controllers/LoginController';
import { exporPlanoEmpresa } from '../../Controllers/EmpresaController';
import { listarModalidades, listarMembro } from '../../Controllers/ExercicioController';

const usuario = (openApi) => {    
    openApi.post('/registrar-aluno',validator('registrar-aluno'),verify, registrarAluno);
    openApi.post('/registrar-funcionario',validator('registrar-funcionario'),verify, registrarFuncionario);
    openApi.post('/registrar-empresa',validator('registrar-empresa'),verify, registrarEmpresa);
    openApi.get('/get-empresa',validator('export-plano-empresa'),verify, exporPlanoEmpresa);
    openApi.get('/get-modalidade', listarModalidades);
    openApi.get('/get-membro', listarMembro);
    openApi.post('/login',login);



    // exercicios
    
}
export default usuario
