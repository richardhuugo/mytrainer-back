
import { verify, validator } from "../../utils/Validator";
import { REGISTRAR_GRUPO } from "../constants";
import {registrarGrupo} from '../../Controllers/GrupoController'
const administrador = (api) => {
    api.post('/registrar-grupo',validator(REGISTRAR_GRUPO),verify, registrarGrupo);
    
}


export default administrador;