import { registrarTipo } from "../../Controllers/AdministradorController";
import { verify, validator } from "../../utils/Validator";
import { REGISTRAR_TIPO_USER, REGISTRAR_MODALIDADE, REGISTRAR_MEMBRO, REGISTRAR_EXERCICIO } from "../constants";
import { registrarModalidades, createMembro, registrarExercicio } from "../../Controllers/ExercicioController";

const administrador = (api) => {
    api.post('/registrar-tipo-usuario',validator(REGISTRAR_TIPO_USER),verify, registrarTipo);
    api.post('/registrar-modalidade',validator(REGISTRAR_MODALIDADE),verify, registrarModalidades);
    api.post('/registrar-membro',validator(REGISTRAR_MEMBRO),verify, createMembro);
    api.post('/registrar-exercicio',validator(REGISTRAR_EXERCICIO),verify, registrarExercicio);
}


export default administrador;