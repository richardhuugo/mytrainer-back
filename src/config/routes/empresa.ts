import { registrarTipo } from "../../Controllers/AdministradorController";
import { verify, validator } from "../../utils/Validator";
import { REGISTRAR_TIPO_USER } from "../constants";
import { registrarFuncionario, listarFuncionario, registrarAluno, listarAluno, updateFuncionario, deletarAluno, registrarPlanoEmpresaAluno, excluirPlanoEmpresaAluno, listarPlanosEmpresaAluno, registrarPacoteAluno, excluirPacoteAluno, listarPacotesAlunos, buscarFuncionario, capturarInfoAluno } from "../../Controllers/EmpresaController";

const empresa = (api) => {
    
    //FUNCIONARIO
    api.post('/registrar-funcionario',validator('registrar-funcionario-empresa'),verify, registrarFuncionario);
    api.put('/update-funcionario',validator('update-funcionario-empresa'), verify,updateFuncionario);
    api.get('/listar-funcionarios',listarFuncionario)
    api.post('/detalhe-usuario',validator('detalhe-usuario'), verify, buscarFuncionario)
    // ALUNO
    api.get('/capturar-informacao-usuario', validator('capturar-info-usuario'), verify, capturarInfoAluno)
    api.post('/registrar-aluno',validator('registrar-aluno-empresa'),verify, registrarAluno);
    api.get('/listar-aluno',listarAluno);
    api.delete('/deletar-aluno',validator('deletar-aluno-empresa'), verify, deletarAluno)
    // PLANO
    api.post('/registrar-plano',validator('registrar-plano-empresa'),verify, registrarPlanoEmpresaAluno);
    api.put('/update-plano', validator('update-plano-empresa'),verify, excluirPlanoEmpresaAluno)
    api.get('/listar-plano',listarPlanosEmpresaAluno)
    //PACOTE
    
    api.post('/registrar-pacote-aluno', validator('registrar-pacote-aluno'),verify,registrarPacoteAluno);
    api.delete('/deletar-pacote-aluno', validator('deletar-pacote-aluno'),verify,excluirPacoteAluno)
    api.get('/listar-pacote-aluno', listarPacotesAlunos)
}


export default empresa;