import * as  express from 'express';
import usuario from './usuario';
import administrador from './administrador';
import empresa from './empresa';
import EmpresaAuth from '../auth/EmpresaAuth';
export const index = (server) => {
    const openApi = express.Router()
    server.use('/v1',openApi) 
    const empresaApi= express.Router()
  
    server.use('/v1/empresa',empresaApi) 
    const funcionarioApi= express.Router()
    server.use('/v1/funcionario',funcionarioApi) 
    const alunoApi= express.Router()
    server.use('/v1/aluno',alunoApi) 
    const administradorApi= express.Router();
    server.use('/v1/admin',administradorApi) 

    usuario(openApi);

    administrador(administradorApi);
    
    empresaApi.use(EmpresaAuth);
    empresa(empresaApi)
}