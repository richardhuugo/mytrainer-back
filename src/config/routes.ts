const express = require('express')
import auth from './auth/AlunoAuth';
import { open } from 'fs';
//import {loginUser, validateToken, registrarUser} from '../services/HomeService';
 
/**import {registrarTpUserUsuario, 
  listarTpUser,
  registrarMembro, 
  listarMembros, 
  listarExercicios, 
  registrarExercicio,
  listarEmpresas,
  registrarPacote,
  listarPacote,
  registrarPlanoAdesao,
  listarPlanoAdesao,
  registrarPlanoVigencia,
  listarPlanoVigencia,
  registrarCategoria,
  listarCategoria,
  registrarSemana,
  listarSemana,
  listarFuncionariosEmpresa,

} from '../services/Admin/adminService';
import { registrarAlunoFuncionario, listarAlunoFuncionario, listarFuncionarios, registrarFuncionarioAtv, listarFuncionrioAtividade, listarVendas } from '../services/Funcionario/FuncionarioService';
import { registrarGrupo, listarGrupo, registrarObjetivoTreino, listarObjetivoTreino, registrarPlanoAlunoEmpresa, listarPlanoAlunoEmpresa, registrarPlano } from '../services/Treino/TreinoService';
import { listarAlunos, meusPagamento } from '../services/Aluno/AlunoService';
import { registrarEmpresa,listarAlunoAcademia,  registrarFuncionarioEmpresa, registrarAlunoAcademia } from '../services/Empresa/EmpresaService';

  */
const  routes = (server)  => {
    
    // ROTAS PROTEGIDAS
    const protectedApi = express.Router()
    server.use('/close', protectedApi)
    protectedApi.use(auth)
    /** rotas de administrador
      // registrar tipo de usuario e listar
      protectedApi.post('/registrar-tipo-usuario',  registrarTpUserUsuario );
      protectedApi.get('/listar-tipo-usuario', listarTpUser );
      // registrar membro e listar
      protectedApi.post('/registrar-membro',  registrarMembro );        
      protectedApi.get('/listar-membro',  listarMembros );
      // registrar exercicios e listar 
      protectedApi.post('/registrar-exercicio', registrarExercicio );        
      protectedApi.get('/listar-exercicio', listarExercicios );

      //listar empresas
      protectedApi.get('/listar-empresas',  listarEmpresas );
      //registrar pacote adesao
      protectedApi.post('/registrar-pacote',  registrarPacote );
      //listar pacotes
      protectedApi.get('/listar-pacote', listarPacote );
      //registrar plano adesao
      protectedApi.post('/registrar-plano-adesao', registrarPlanoAdesao );
      //listar plano adesao
      protectedApi.get('/listar-plano-adesao', listarPlanoAdesao);
      //registrar plano vigencia empresa
      protectedApi.post('/registrar-plano-vigencia',registrarPlanoVigencia);
      //listar plano vigencia empresa 
      protectedApi.get('/listar-plano-vigencia',listarPlanoVigencia);
      // registrar categoria 
      protectedApi.post('/registrar-categoria', registrarCategoria)
      // listar categoria
      protectedApi.get('/listar-categoria', listarCategoria)
      // registrar dia semana
      protectedApi.post('/registrar-semana', registrarSemana)
      // listar dia semana
      protectedApi.get('/listar-semana',listarSemana)
      // listar funcionario empresa
      protectedApi.get('/listar-funcionario-empresa', listarFuncionariosEmpresa)

    // rotas funcionario
    // registrar aluno funcionario
    protectedApi.post('/registrar-aluno-funcionario',registrarAlunoFuncionario)
    // listar aluno funcionario
    protectedApi.get('/listar-aluno-funcionario', listarAlunoFuncionario)
    // listar funcionarios
    protectedApi.get('/listar-funcionarios', listarFuncionarios)
    // registrar grupo de exercicios
    protectedApi.post('/registrar-grupo', registrarGrupo)
    //listar grupos de exercicios
    protectedApi.get('/listar-Grupo', listarGrupo);
    //registrar objetivo treino
    protectedApi.post('/registrar-objetivo-treino', registrarObjetivoTreino);
    // listar objetivo treino
    protectedApi.get('/listar-objetivo-treino', listarObjetivoTreino)

    protectedApi.post('/registrar-funcionario-atividade',registrarFuncionarioAtv);
    protectedApi.get('/listar-funcionario-atividades', listarFuncionrioAtividade);
  
    protectedApi.post('/realizar-pagamento', realizarPagamento);

    protectedApi.get('/meus-pagamentos', meusPagamento);

    protectedApi.get('/listar-vendas', listarVendas);


    // registrar plano de exercicios aluno academia
    protectedApi.post('/registrar-plano-exercicios-academia-aluno', registrarPlanoAlunoEmpresa)
    // listar plano de exercicios aluno academia
    protectedApi.get('/listar-plano-aluno-academia', listarPlanoAlunoEmpresa)
    // registrar plano de exercicios
    protectedApi.post('/registrar-plano-exercicios', registrarPlano)
    
    

    // rotas empresa
    
    protectedApi.get('/listar-aluno', listarAlunos)
    protectedApi.post('/register-empresa-funcionario',registrarFuncionarioEmpresa);
    protectedApi.post('/registrar-aluno-empresa',registrarAlunoAcademia);
    protectedApi.get('/listar-aluno-empresa',listarAlunoAcademia); */
    
  /** 
   
    
    
    

    
   */  // configurar rotas privadas
   /** protectedApi.post('/register-plano-vigencia',servico.registrarPlanoVigencia);
    protectedApi.post('/register-plano-adesao',servico.registrarPlanoAdesao);
    
    protectedApi.get('/listar-pacote',servico.listarPacote);
    protectedApi.post('/register-pacote',servico.registrarPacote);
    protectedApi.post('/register-area',servico.registrarArea);
    protectedApi.post('/register-funcionario',servico.registrarFuncionario);
    protectedApi.get('/list-funcionario',servico.listarFuncionarios);
    protectedApi.get('/list-funcionario-empresa',servico.listarFuncionariosEmpresa);
    protectedApi.get('/list-plano-adesao',servico.listarPlanoAdesao);
    protectedApi.get('/list-plano-vigencia',servico.listarPlanoVigencia);
   // protectedApi.post('/register-grupo-funcionario',servico.registrarGrupoFuncionario);
   protectedApi.post('/register-grupo-funcionario',servico.registrarGrupoFuncionario);
   protectedApi.post('/register-funcionario-atividade',servico.registrarGrupoFuncionarioAtv);
   protectedApi.get('/listar-objetivos-treino',servico.ListarObjetivoTreino);
   protectedApi.post('/registrar-atividade-funcionario',servico.registrarFuncionarioAtv);

   
   
    */
   


    // ROTAS ABERTAS
    const openApi = express.Router()
    server.use('/open',openApi)
//    openApi.post('/login-user',loginUser);

    /**
     * 
     *    
     * 
     *    
    openApi.post('/validate',validateToken);
    openApi.post('/registrar-usuario',registrarUser);
    openApi.post('/registrar-empresa', registrarEmpresa)
    
    
    server.use('/open',openApi)



     openApi.post('/registrar-aluno', alunoService.registrarAluno)
     // registrar funcionarios
    
   //openApi.get('/obter',PagamentoFuncao.pagamento )
     openApi.post('/registrar-funcionario', funcionarioService.registrarFuncionario)
     
 openApi.get('/listar-membro',listarMembros);    
    openApi.post('/register-exercicio',registrarExercicio);
    openApi.get('/listar-empresas',listarEmpresas);
    openApi.get('/listar-exercicios',listarExercicios);        
    openApi.post('/register-grupo',registrarGrupo);

    Exercicio.register(openApi, '/exercicio')
    openApi.get('/list-tp-users',servico.listarTpUser);

    openApi.get('/listar-alunos',servico.listarAlunos);
    openApi.get('/list-users',servico.listarUsers);                        
    openApi.post('/registrar-objetivo',servico.registrarObjetivoTreino);    
    openApi.post('/tpuser',servico.registrarTpUser);
    openApi.post('/register-user',servico.registrarUser);
    
    
    
    openApi.post('/register-empresa',servico.registrarEmpresa);
    openApi.post('/register-tp-user',servico.registrarTpUser);

    openApi.post('/register-aluno',servico.registrarAluno); */

    

    
    
   // configurar rotas abertas
    
   
}

export default routes