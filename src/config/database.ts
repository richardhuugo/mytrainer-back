const mongoose = require('mongoose')
mongoose.set('debug', true);

mongoose.Promise = global.Promise
const config = {
    autoIndex: false,
    useNewUrlParser: true,
  };
export default  mongoose.connect('mongodb://usermytrainer:richard2016@ds149593.mlab.com:49593/mytrainer' ,config)

mongoose.Error.messages.general.required = "O atributo '{PATH}' é obrigatório."
mongoose.Error.messages.Number.min = 
    "O '{VALUE}' informado é menor que o limite mínimo de '{MIN}'."
    
mongoose.Error.messages.Number.max = 
    "O '{VALUE}' informado é maior que o limite máximo de '{MAX}'."
mongoose.Error.messages.String.enum = 
    "'{VALUE}' não é válido para o atributo '{PATH}'."