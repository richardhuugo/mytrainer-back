import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

 
const Grupo = new mongoose.Schema({            
    usuario:{ type: Schema.Types.ObjectId, ref: 'User'},      
    exercicios :[{
        exercicio:{ type: Schema.Types.ObjectId, ref: 'Exercicio'},      
        series:{ type: String, required: true },     
        repeticoes:{ type: String, required: true },
        observacao: { type: String, required: false }
    }],          
    grupo:{ type: String, required: true },  
    semana_id:[{ type: Schema.Types.ObjectId, ref: 'Semana'}],           
    observacao: { type: String, required: false },   
    status:{ type:String,  default:'A' },            
    disponivel:{ type:Boolean,  default:false },   
})


    

export default mongoose.model("Grupo", Grupo);