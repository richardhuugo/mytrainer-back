

import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const UsuarioPerfis = new mongoose.Schema({             
    user:{type: Schema.Types.ObjectId, ref: 'Usuario'},
    tipo: { type: Schema.Types.ObjectId, ref: 'Tipo' },     
    status:{ type:String,  default:'A'},  
},{
    timestamps:true
})

export default mongoose.model("UsuarioPerfis", UsuarioPerfis);