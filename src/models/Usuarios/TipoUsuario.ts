import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let TipoUsuario = new Schema({
    tipo: { type: String, required: true },    
});

export default mongoose.model('Tipo', TipoUsuario);
