import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

let Usuario = new Schema({
    nome: { type: String, required: true },
    sobrenome: { type: String, required: true },
    email: { type: String, required: true },
    senha: { type: String, required: true },
    documento: {type: String, required:false},
    status: { type: String, default: 'A' },
}, {
        timestamps: true
    });

export default mongoose.model('Usuario', Usuario);
