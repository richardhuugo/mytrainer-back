import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Cidade = new mongoose.Schema({        
    cidade:{type:String, required:false},   
    estado:{type:String, required:true}         
} )

export default mongoose.model("Cidade", Cidade);