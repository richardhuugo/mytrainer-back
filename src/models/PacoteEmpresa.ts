import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PacoteEmpresa = new mongoose.Schema({        
    descricao:{type:String, required:true}     ,
    status:{type:String, default:'A'}     ,
    plano:{ type: Schema.Types.ObjectId, ref: 'PlanoAdesaoEmpresa'}
})

export default mongoose.model("PacoteEmpresa", PacoteEmpresa);