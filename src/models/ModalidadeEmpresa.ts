import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const ModalidadeEmpresa = new mongoose.Schema({   
    modalidade_id:{ type: Schema.Types.ObjectId, ref: 'Modalidade'} ,     
    descricao:{type:String, required:true},
    imagem:[{ type: Schema.Types.ObjectId, ref: 'Imagem'}]    
},{
    timestamps:true
})

export default mongoose.model("ModalidadeEmpresa", ModalidadeEmpresa);