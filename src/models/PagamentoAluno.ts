import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PagamentoAluno = new mongoose.Schema({        
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},
    user:{ type: Schema.Types.ObjectId, ref: 'Usuario'},
    response:{type:Date, required:true} 
},{
    timestamps:true
})

export default mongoose.model("PagamentoAluno", PagamentoAluno);