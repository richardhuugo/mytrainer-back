import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PlanoTreinoEmpresa = new mongoose.Schema({      
    aluno:{ type: Schema.Types.ObjectId, ref: 'EmpresaAluno'},     
    instrutor:{type: Schema.Types.ObjectId, ref: 'EmpresaFuncionario'},
    grupo:[{ type: Schema.Types.ObjectId, ref: 'Grupo'}],                   
    observacao: { type: String, required: false },   
    status:{ type:String,  default:'A' },  
},{
    timestamps:true
})

export default mongoose.model("PlanoTreinoEmpresa", PlanoTreinoEmpresa);