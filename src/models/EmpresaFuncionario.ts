import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const EmpresaFuncionario = new mongoose.Schema({        
    empresa_id:{ type: Schema.Types.ObjectId, ref: 'Empresa'},        
    user:{ type: Schema.Types.ObjectId, ref: 'Usuario'},  
    status:{ type:String,  default: 'A' },  
},{
    timestamps:true
})

export default mongoose.model("EmpresaFuncionario", EmpresaFuncionario);