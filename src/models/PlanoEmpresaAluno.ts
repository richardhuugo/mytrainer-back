import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PlanoEmpresaAluno = new mongoose.Schema({        
    empresa_id:{ type: Schema.Types.ObjectId, ref: 'Empresa'} ,
    descricao:{type:String, required:true},
    valor:{type:String, required:true},
    periodo:{type:String, required:true, uppercase: true,
        enum: ['MENSAL', 'BIMESTRAL', 'SEMESTRAL','ANUAL']},
    status:{type:String, default:'A',required:true, uppercase: true,
    enum: ['A', 'I']}     
},{
    timestamps:true
})

export default mongoose.model("PlanoEmpresaAluno", PlanoEmpresaAluno);