import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PlanoTreinoFuncionario = new mongoose.Schema({       
    aluno:{type: Schema.Types.ObjectId, ref: 'user'},
    instrutor:{type: Schema.Types.ObjectId, ref: 'user'},
    grupo:[{ type: Schema.Types.ObjectId, ref: 'Grupo'}],       
    observacao: { type: String, required: false },   
    status:{ type:String,  default:'A' },  
},{
    timestamps:true
})

export default mongoose.model("PlanoTreinoFuncionario", PlanoTreinoFuncionario);