import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PlanoAdesaoEmpresa = new mongoose.Schema({        
    descricao:{type:String, required:true}  ,   
    valor:{type:String, required:true}  ,
    dias:{type:String, required:true}  ,
    status:{type:String, default:'A'}     
},{
    timestamps:true
})

export default mongoose.model("PlanoAdesaoEmpresa", PlanoAdesaoEmpresa);