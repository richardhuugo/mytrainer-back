import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Endereco = new mongoose.Schema({        
    cep:{type:String, required:false},   
    bairro:{type:String, required:true},              
    cidade_id:{ type: Schema.Types.ObjectId, ref: 'Cidade'},         
})

export default mongoose.model("Endereco", Endereco);