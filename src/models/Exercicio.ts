import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Exercicios = new mongoose.Schema({            
    exercicio: { type: String, required: true },  
    membro:{ type: Schema.Types.ObjectId, ref: 'Membro'},
    status:{ type:String,  default:'A' },     
    modalidades:[{ type: Schema.Types.ObjectId, ref: 'Modalidades'}]          
})
 
 
export default mongoose.model("Exercicio", Exercicios);