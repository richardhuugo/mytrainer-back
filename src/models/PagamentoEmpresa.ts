import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PagamentoEmpresa = new mongoose.Schema({        
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},
    plano_vigencia_empresa:{ type: Schema.Types.ObjectId, ref: 'PlanoVigenciaEmp'},
    response:{type:Date, required:true} 
},{
    timestamps:true
})

export default mongoose.model("PagamentoEmpresa", PagamentoEmpresa);