import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PacoteAluno = new mongoose.Schema({        
    plano:{ type: Schema.Types.ObjectId, ref: 'PlanoEmpresaAluno'} ,
    user:{ type: Schema.Types.ObjectId, ref: 'Usuario'},
    desconto:{type:Number, default:0},        
    status:{type:String, default:'A'}     
},{
    timestamps:true
})

export default mongoose.model("PacoteAluno", PacoteAluno);