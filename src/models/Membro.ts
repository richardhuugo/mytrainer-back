import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Membro = new mongoose.Schema({            
    membro: { type: String, required: true },             
})

export default mongoose.model("Membro", Membro);