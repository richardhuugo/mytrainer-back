import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Modalidades = new mongoose.Schema({            
    modalidades: { type: String, required: true }     ,
    descricao: { type: String, required: true }           
})
 
 
export default mongoose.model("Modalidades", Modalidades);