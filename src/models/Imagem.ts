import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Imagem = new mongoose.Schema({        
    imagem:{type:String, required:true}    ,
    modalidade_id:{ type: Schema.Types.ObjectId, ref: 'Modalidades'} ,
    empresa_id:{ type: Schema.Types.ObjectId, ref: 'Empresa'} 
})

export default mongoose.model("Imagem", Imagem);