import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Modalidade = new mongoose.Schema({        
    modalidade:{type:String, required:true}    
})

export default mongoose.model("Modalidade", Modalidade);