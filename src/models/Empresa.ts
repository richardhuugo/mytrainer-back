import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Empresa = new mongoose.Schema({        
    nick:{type:String, required:false},   
    nm_fantasia:{type:String, required:true},    
    razao_social: { type: String, required: true },
    cnpj:{type:String, required:true},          
    endereco:{ type: Schema.Types.ObjectId, ref: 'Endereco'}    
},{
    timestamps:true
})

export default mongoose.model("Empresa", Empresa);