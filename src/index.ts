import {server, app} from './config/server';
import './config/database';
import {index} from './config/routes/index';
index(app)
export default {server}

