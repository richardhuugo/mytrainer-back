import {check,body, validationResult} from 'express-validator/check'
import { REGISTRAR_TIPO_USER, REGISTRAR_MODALIDADE, REGISTRAR_MEMBRO, REGISTRAR_EXERCICIO, REGISTRAR_GRUPO } from '../config/constants';
import Usuario from '../models/Usuarios/Usuario';
import TipoUsuario from '../models/Usuarios/TipoUsuario';
import Empresa from '../models/Empresa';
import EmpresaFuncionario from '../models/EmpresaFuncionario';
import Modalidades from '../models/Modalidades';
import Membro from '../models/Membro';
import Exercicio from '../models/Exercicio';

export const validator =    (method) => {
  
    switch (method) {
        case 'registrar-aluno':  {return similar();}          
        case 'registrar-funcionario': {return similar();}    
        case 'detalhe-usuario': {
          return [
            body('documento').not().isEmpty().withMessage('Documento inválido')
          ]
        }  
        case REGISTRAR_GRUPO : {
          return [
            
          ]
        }
        case REGISTRAR_MEMBRO : {
          return [
            body('membro').not().isEmpty().withMessage('Membro inválido').custom(value =>{
              return Membro.findOne({membro:value}).then(membro => {
                if(membro){
                  return Promise.reject('Membro já cadastrado no sistema');
                }
              })
            })
          ]
        }
        case REGISTRAR_EXERCICIO: {
          return [
            body('exercicio').not().isEmpty().withMessage('Exercicio inválido').custom(value =>{
              return Exercicio.findOne({exercicio:value}).then(exercicio => {
                if(exercicio){
                  return Promise.reject('Exercicio já cadastrado no sistema');
                }
              })
            }),
            body('membro_id').not().isEmpty().isMongoId().withMessage('Membro inválido'),
            body('modalidade_id').not().isEmpty().isMongoId().withMessage('Modalidade inválida')
          ]
        }
        case REGISTRAR_MODALIDADE : {
          return [ 
            body('modalidade').not().isEmpty().withMessage('Modalidade inválida').custom(value =>{
              return Modalidades.findOne({modalidades:value}).then(modalidade => {
                if(modalidade){
                  return Promise.reject('Modalidade já cadastrado no sistema');
                }
              })
            }),
            body('descricao').not().isEmpty().withMessage('Descrição inválida')
          ] 
        }             
        case 'registrar-empresa': {
          return [ 
            body('nm_fantasia').not().isEmpty().withMessage('Nome fantasia inválido'),
            body('razao_social').not().isEmpty().withMessage('Razão social inválida'),
            body('cnpj').not().isEmpty().withMessage('CNPJ inválido').custom(value =>{
              return Empresa.findOne({cnpj:value}).then(empresa => {
                if(empresa){
                  return Promise.reject('CNPJ já cadastrado no sistema');
                }
              })
            }),
            body('cep').not().isEmpty().withMessage('CEP inválido'),
            body('cidade').not().isEmpty().withMessage('Cidade inválida'),
            body('bairro').not().isEmpty().withMessage('Bairro inválido'),         
            body('email').isEmail().withMessage('E-mail inválido') ,
            body('senha').not().isEmpty().withMessage('Senha inválida')
          ]   
        } 
        // EMPRESA
        case 'registrar-aluno-empresa':  {
          return [
            body('user_id').optional().isMongoId().withMessage('Informe um ID válido'),
            body('nome').optional().isString().withMessage('Nome inválido'),
            body('sobrenome').optional().isString().withMessage('Sobrenome inválido'),
            body('email').optional().isEmail().withMessage('E-mail inválido') ,           
            body('documento').optional().withMessage('Documento inválido')      
          ]
        }    
        case 'registrar-funcionario-empresa': {
          return [  
            body('user_id').optional().isMongoId().withMessage('Informe um ID válido'),
            body('nome').optional().isString().withMessage('Nome inválido'),
            body('sobrenome').optional().isString().withMessage('Sobrenome inválido'),
            body('email').optional().isEmail().withMessage('E-mail inválido') , 
            body('documento').optional().withMessage('Documento inválido') 
          ]  
        }
        case 'update-funcionario-empresa':{
          return [
            body('user_id').not().isEmpty().isMongoId().withMessage('Informe um ID válido'),            
            body('statusAtualizar').not().isEmpty().withMessage('Informe um status válido')            
          ]
        }
        case 'deletar-aluno-empresa':{
         return [
          body('user_id').not().isEmpty().isMongoId().withMessage('Informe um ID válido'),
          body('statusAluno').not().isEmpty().withMessage('Informe um status válido')
         ]
        }
        case 'registrar-plano-empresa': {
          return [
            body('descricao').not().isEmpty().withMessage('Informe uma descrição válida'),
            body('valor').not().isEmpty().withMessage('Informe um valor válido'),
            body('periodo').not().isEmpty().withMessage('Informe um periodo válido')
          ]
        }
        case 'update-plano-empresa':{
          return [
            body('plano_id').not().isEmpty().isMongoId().withMessage('Informe um ID válido'),
            body('statusPlano').not().isEmpty().withMessage('Informe um status válido')
          ]
        }
        case 'registrar-pacote-aluno':{
          return [
            body('plano_id').not().isEmpty().isMongoId().withMessage('Informe um plano válido'),
          body('user_id').not().isEmpty().isMongoId().withMessage('Informe um aluno válido')
          ]
        }
        case 'deletar-pacote-aluno':{
          return [
            body('plano_id').not().isEmpty().isMongoId().withMessage('Informe um plano válido'),
          body('user_id').not().isEmpty().isMongoId().withMessage('Informe um aluno válido')
          ]
        }
        case 'capturar-info-usuario':{
          return [
            check('cpf').not().isEmpty().withMessage('Informe um documento válido') ,
            check('type').not().isEmpty().withMessage('Informe um tipo válido') ,
                     
          ]
        }
        case 'export-plano-empresa':{
          return [
            check('empresa_id').not().isEmpty().withMessage('Informe uma empresa válida') ,         
          ]
        }
        case REGISTRAR_TIPO_USER: {
          return [  check('tipo').not().isEmpty().isString().custom(value => {
            return TipoUsuario.findOne({tipo:value}).then(tipo => {
              if (tipo) {
                return Promise.reject('Tipo já cadastrado no sistema');
              }
            })
          }) ]  
                                     
        }
        default:
          return []
      }
}
const similar = () => {
  return [ 
    check('nome').not().isEmpty().withMessage('Informe o nome'),
    check('sobrenome').not().isEmpty().withMessage('Informe o sobrenome'),
    check('email').isEmail().withMessage('E-mail inválido'),
    check('documento').not().isEmpty().withMessage('Informe um documento valido'),
    check('senha').not().isEmpty().withMessage('Informe a senha')
    ];
}
export const verify = (req, res, next) => {
    req.getValidationResult()    
    .then(() => {           
     const errors = validationResult(req);         
     if (!errors.isEmpty()) 
       return res.status(422).json({ errors: errors.array() });
    
       next();                        
    })    
    .catch(() => {
      const errors = validationResult(req);     
     return  res.json(errors.array())
    })
}