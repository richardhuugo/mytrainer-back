import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Avaliacao = new mongoose.Schema({       
    user_aluno:{ type: Schema.Types.ObjectId, ref: 'User'},  
    avaliador:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},  
    proximaData:{type:String, required:true},    
    data:{type:String, required:true},    
    peso: { type: String, required: true },
    altura: { type: String, required: true },
    torax: { type: String, required: true },
    braco_esq: { type: String, required: true },
    braco_dir: { type: String, required: true },
    cintura: { type: String, required: true },
    abdomen: { type: String, required: true },
    quadril: { type: String, required: true },
    coxa_dir: { type: String, required: true },
    coxa_esq: { type: String, required: true },
    antebraco_esq: { type: String, required: true },
    antebraco_dir: { type: String, required: true },
    panturrilha_esq: { type: String, required: true },
    panturrilha_dir: { type: String, required: true },
    gordura_percentual: { type: String, required: true },
    peso_gordura: { type: String, required: true },
    massa_magra: { type: String, required: true },
    peso_residual: { type: String, required: true },
    peso_muscular: { type: String, required: true },
    peso_percentual: { type: String, required: true },

},{
    timestamps:true
})

export default mongoose.model("Avaliacao", Avaliacao);