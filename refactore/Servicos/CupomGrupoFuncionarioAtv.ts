import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const cupomGrupoFuncionarioAtv = new mongoose.Schema({             
    grupo_func:{ type: Schema.Types.ObjectId, ref: 'GrupoFuncionarioAtividades'}, 
    cupom:{ type: Schema.Types.ObjectId, ref: 'Cupom'}, 
      
} )

export default mongoose.model("cupomGrupoFuncionarioAtv", cupomGrupoFuncionarioAtv);