import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const cupomFuncionarioAtv = new mongoose.Schema({             
    grupo_func:{ type: Schema.Types.ObjectId, ref: 'FuncionarioAtividades'}, 
    cupom:{ type: Schema.Types.ObjectId, ref: 'Cupom'},  
      
} )

export default mongoose.model("cupomFuncionarioAtv", cupomFuncionarioAtv);