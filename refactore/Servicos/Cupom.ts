import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Cupom = new mongoose.Schema({             
    titulo:{type:String, required:true},    
    descricao:{type:String, required:true},    
    dt_inicio:{type:Date, required:true},
    dt_fim:{type:Date, required:true},
    codigo:{type:String, required:true},        
    status:{type:String, default:'A'},    
} )

export default mongoose.model("Cupom", Cupom);