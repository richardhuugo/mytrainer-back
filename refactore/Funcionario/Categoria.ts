import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Categoria = new mongoose.Schema({        
    categoria: { type: String, required: true },   
} )

export default mongoose.model("Categoria", Categoria);