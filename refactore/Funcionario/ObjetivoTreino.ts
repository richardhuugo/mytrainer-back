import * as mongoose from 'mongoose';

const ObjetivoTreino = new mongoose.Schema({        
   
    objetivos: { type: String, required: true },
     
},{
    timestamps:true
})

export default mongoose.model("ObjetivoTreino", ObjetivoTreino);