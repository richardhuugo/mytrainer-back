import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const GrupoFuncionario = new mongoose.Schema({        
    funcionario_id:[{ type: Schema.Types.ObjectId, ref: 'Funcionario'}],          
    create_id:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},
    descricao_grupo:{type:String, required:true},       
    status:{ type:String,  default: 'A' }, 
},{
    timestamps:true
})

export default mongoose.model("GrupoFuncionario", GrupoFuncionario);