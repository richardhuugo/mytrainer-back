import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const FuncionarioAtividades = new mongoose.Schema({        
    funcionario_id:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},     
    valor: { type: String, required: true },
    treino_obj:[{ type: String}],     
    descricao:{type:String, required:true},   
    status:{ type:String,  default:'A' },             
},{
    timestamps:true
})

export default mongoose.model("FuncionarioAtividades", FuncionarioAtividades);