import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const GrupoFuncionarioAtividades = new mongoose.Schema({            
    grupofun_id:{ type: Schema.Types.ObjectId, ref: 'GrupoFuncionario'},     
    obj_treino:[{ type: Schema.Types.ObjectId, ref: 'ObjetivoTreino'}],         
    valor: { type: String, required: true },
    descricao:{type:String, required:true},    
    status:{ type:String,  default: 'A' },         
   
},{
    timestamps:true
})

export default mongoose.model("GrupoFuncionarioAtividades", GrupoFuncionarioAtividades);