import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const AlunoFuncionario = new mongoose.Schema({        
    funcionario_id:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},        
    aluno_id:{ type: Schema.Types.ObjectId, ref: 'Aluno'},  
    status:{ type:String,  default: 'A' },  
},{
    timestamps:true
})

export default mongoose.model("AlunoFuncionario", AlunoFuncionario);