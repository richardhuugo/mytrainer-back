import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;
const Funcionario = new mongoose.Schema({        
    user_id:{ type: Schema.Types.ObjectId, ref: 'User'},       
    descricao_pessoal:{type:String, require:false},           
    status:{ type:String,  default:'A'}, 
},{
    timestamps:true
})

export default mongoose.model("Funcionario", Funcionario);