import * as mongoose from 'mongoose'
let Schema = mongoose.Schema;

const Area = new mongoose.Schema({        
    funcionario_id:{ type: Schema.Types.ObjectId, ref: 'Funcionario'},     
    categoria_id:{ type: Schema.Types.ObjectId, ref: 'Categoria'},     
    num_categoria: { type: String, required: true },
    status:{ type:String,  required: true },  
},{
    timestamps:true
})

export default mongoose.model("Area", Area);