import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Header = new mongoose.Schema({        
      
    aluno:{ type: Schema.Types.ObjectId, ref: 'Aluno'},            
    funcionarioAtv:{ type: Schema.Types.ObjectId, ref: 'FuncionarioAtividades'},     
    grupoFuncionarioAtv:{ type: Schema.Types.ObjectId, ref: 'GrupoFuncionarioAtividades'},     
    status:{ type:String,  default:'A' },             
}  )

export default mongoose.model("Header", Header);