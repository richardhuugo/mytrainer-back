import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;
 
const Message = new mongoose.Schema({            
    header:{ type: Schema.Types.ObjectId, ref: 'Header'},     
    message:{  type:String,  required: true },  
    status:{ type:String,  default: 'A' },         
   
},{
    timestamps:true
})

export default mongoose.model("Message", Message);

 