import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const Pagamento = new mongoose.Schema({              
    aluno:{ type: Schema.Types.ObjectId, ref: 'Aluno'},         
    retornoPagamento:{type:String, required:false},       
    funcionarioAtv:{ type: Schema.Types.ObjectId, ref: 'FuncionarioAtividades'},     
    funcionarioGrupoAtv:{ type: Schema.Types.ObjectId, ref: 'GrupoFuncionarioAtividades'},   
    status:{ type:String,  default:'A' },             
} )

export default mongoose.model("Pagamento", Pagamento);