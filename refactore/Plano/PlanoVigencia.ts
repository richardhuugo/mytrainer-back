import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const PlanoVigencia = new mongoose.Schema({        
   
    data_inicio:{type:Date, required:true}  ,
    data_fim:{type:Date, required:true}  ,    
    empresa:{ type: Schema.Types.ObjectId, ref: 'Empresa'},
    pacote:{ type: Schema.Types.ObjectId, ref: 'Pacote'},
    status:{type:String, default:'A'}  
},{
    timestamps:true
})

export default mongoose.model("PlanoVigencia", PlanoVigencia);