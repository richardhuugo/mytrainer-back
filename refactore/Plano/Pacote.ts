import * as mongoose from 'mongoose';
let Schema = mongoose.Schema;

const pacote = new mongoose.Schema({        
    descricao:{type:String, required:true}     ,
    status:{type:String, default:'A'}     ,
    plano:{ type: Schema.Types.ObjectId, ref: 'PlanoAdesao'}
})

export default mongoose.model("Pacote", pacote);